//======================================================================
// Uses
//======================================================================
use <std/motors.scad>
use <std/GT2Gear.scad>
use <std/lmxuu.scad>
//
use <x/xmounts.scad>
use <hotend.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

//======================================================================
// main program, comment components as needed
//======================================================================

xaxis(length=420,endsep=345,barsep=_xrodZsep,px = 0,car=0,dzr=0);

//======================================================================
// modules
//======================================================================

module xaxis(length=500, endsep=450, barsep=_xrodZsep, depth=45, px=0, car=1, dzr=_xrodoffsetz){

     lm = _xdiam;
     dxrods = _rodlr2-_zrodoffsetx;// lm8 11.5, lm10 13.5
     dyrods = 7+lm;//// lm8 15, lm10 17
     dzrods = 26-lm;//// lm8 18, lm10 20
     translate([endsep/2,0,0]) {
         xmotorend(swap=-1);
         translate([ dxrods,dyrods, dzrods])rotate([90,0,0])lmxuu(lm);
         translate([ dxrods,dyrods,-dzrods])rotate([90,0,0])lmxuu(lm);
     }
     translate([-endsep/2,0,0]) {
         xidleend(swap=-1);
         translate([-dxrods,dyrods, dzrods])rotate([90,0,0])lmxuu(lm);
         translate([-dxrods,dyrods,-dzrods])rotate([90,0,0])lmxuu(lm);
     }

     rn = 12;

translate([0,0,0]){
color("black")
difference(){
hull(){
    translate([-endsep/2-4,-3,0])rotate([-90,0,0]){
        cylinder(d=rn+1,h=6);
    }
    translate([endsep/2+65,-3,0])rotate([-90,0,0]){
        cylinder(d=rn+1,h=6);
    }
}


hull(){
    translate([-endsep/2-4,-3,0])rotate([-90,0,0]){
        cylinder(d=rn-0.5,h=7);
    }
    translate([endsep/2+65,-3.5,0])rotate([-90,0,0]){
        cylinder(d=rn-0.5,h=7);
    }
}
}// diff

}

    if( car == 1)
        translate([px,10.5,-34]) rotate([0,0,90])hotend(rods=barsep);

    color("silver")translate([(endsep)/2+depth,0,-(barsep/2)-dzr]) rotate([0,-90,0])
        cylinder(d=8,h=length, $fn=30);
    color("silver")translate([(endsep)/2+depth,0,(barsep/2)+dzr]) rotate([0,-90,0])
        cylinder(d=8,h=length, $fn=30);

}
