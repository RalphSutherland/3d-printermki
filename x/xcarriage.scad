/*
 * xcarriage.scad
 * v1.0.3  
 *
 * This script is licensed under the Creative Commons - Attribution license.
 *
 * Remix of
 * http://www.thingiverse.com/thing:1585254
 *  by landie @ Thingiverse (Dave White)
 * belt grip tooth phased to prevent assymetric teeth and half teeth,
 * plus bolt blocks extened to  allow for rear mount options , such as a rear blower.
 *
 * Original design based on the Prusa Rework .stl file thing # 119616
 *
 * The three parameters below determine the number of bearings per axle (1 or 2)
 */
//======================================================================
// Uses
//======================================================================
// none!
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================
include <../const.scad>
//======================================================================

// in const.scad _xrodZsep     = 44.5; 
axis_bar_centres = _xrodZsep;//44.5/45
 echo("axis_bar_centres",axis_bar_centres);
// in const.scad _carriageBoltSpacing = 24.0
bolt_y_centres = _carriageBoltSpacing;//24std,24.25,23
bolt_x_centres = _carriageBoltSpacing;//24std,24.25,23

//Number of bearings per axle (1 or 2)
axle_bearings   = 2;
tiewraps = true;

//======================================================================
// Common Variables

carriage_width  = 56;
carriage_height = 70;
carriage_corner_radius = 3;
carriage_block_depth   = 4;

belt_clamp_gap  =  1.0; // black GT2 belt: 1.0; White steel GT2 belt 1.35

//======================================================================
// You should only edit anything below here if you have an idea what you are doing ! :)

//======================================================================
// Mounting hole diameters and spacing
bolt_hole_dia   = 3.7; // migrating to std metric <std/fasteners.scad>
bolt_hole_z     = 5.0;
bolt_block_dia  = 5.75;

//======================================================================
// Variables for the two bearing blocks
bearing_block_height = 26.0; //25,24.5  y
bearing_block_depth  = 7.75;
bearing_block_extra  = 0.00;
bearing_block_corner_radius = 3;

// backing plate
plate_width  = 64;
plate_height = 90;
plate_thick  = 3.0;

//======================================================================
// Variables for the two belt clamps
belt_clamp_y         =   6.36619772367581; //(GT2 20 teeth = 20/pi)
belt_clamp_height    =   8.5;
belt_clamp_width     =    24;
belt_clamp_chamfer   =     2;
belt_clamp_depth     =    11;
belt_tooth_diameter  =  1.25;
belt_tooth_depth     =  1.75;
belt_clamp_end_gap   =     4;
belt_pitch           =     2;
belt_clamp_plate_thick =   4;

//======================================================================
// variables for the axis bearing cutouts LM8UU
axis_bearing_dia = 15.5;
axis_bearing_length = 45.5;
axis_bearing_collar_dia = 13;
//axis_bearing_collar_dia = 15;
axis_bearing_z = bearing_block_depth + carriage_block_depth - 1;
//axis_bearing_z = carriage_block_depth + 5;
twin_bearing_length = 24.5;
twin_bearing_offset =    1;

//======================================================================
// Variables for the tie wrap cutouts/slots
tiewrap_depth = 1.5;
tiewrap_inner_height  = 18;
tiewrap_bottom_offset = 1;
tiewrap_radius = 2.5;
tiewrap_width  = 4.0;
tiewrap_inset  = 6.0;

//======================================================================
// Utility variable used to simplify some of the translations
total_depth = carriage_block_depth + bearing_block_depth;

//======================================================================
// main program, comment components as needed
//======================================================================

x_carriage();

/*
   translate([   0,0,34]) rotate([ 90,0,-90]) 
      x_carriage();
  translate([   6,0,-5]) rotate([  0,0, 90]) 
      x_plate(-5);
*/


//======================================================================
// modules
//======================================================================

module x_carriage() {
    difference() {
        union(){
            x_main_block();
            x_bolt_blocks();
        }
        union() {
            x_bolt_holes();
            x_bearing_cutouts();
        }
    }
}

module x_plate(zoff=0,ChimeraMount=0){
    echo("x_plate dz=",0.5*(68 -plate_height)-zoff);
    color("lightgray")
    translate([0,(plate_thick/2)+0.5,plate_height/2]) rotate([90,0,0]){
        difference(){
            cube([plate_width,plate_height,plate_thick],true);
            if (ChimeraMount==1)
                for(i=[[ 4.5,-0.5*plate_height+20,-5],[-4.5,-0.5*plate_height+20,-5],[0,-0.5*plate_height+10,-5]])
                    translate(i) cylinder(d=2,h=10);
            translate([0,0.5*(68 -plate_height)-zoff,-12]) x_bolt_holes();
         }
        /*
         difference(){
            cube([plate_width,plate_height,plate_thick],true);
            #cube([plate_width-4,plate_height-4,plate_thick+2],true);
        }
         */
    }

}

module x_main_block()
{
    linear_extrude(carriage_block_depth)
    hull() {
        translate([carriage_width / 2 - carriage_corner_radius, carriage_height / 2 - carriage_corner_radius, 0])
            circle(r = carriage_corner_radius);
        translate([-carriage_width / 2 + carriage_corner_radius, carriage_height / 2 - carriage_corner_radius, 0])
            circle(r = carriage_corner_radius);
        translate([carriage_width / 2 - carriage_corner_radius, -carriage_height / 2 + carriage_corner_radius, 0])
            circle(r = carriage_corner_radius);
        translate([-carriage_width / 2 + carriage_corner_radius, -carriage_height / 2 + carriage_corner_radius, 0])
            circle(r = carriage_corner_radius);
    }
    translate([0, -carriage_height / 2 + bearing_block_height / 2, carriage_block_depth])
        x_bearing_block();
    translate([0, carriage_height / 2 - bearing_block_height / 2, carriage_block_depth])
        x_bearing_block();

 //   translate([carriage_width / 2 - belt_clamp_width, -belt_clamp_height + 2.5, carriage_block_depth])

        translate([belt_clamp_end_gap / 2,+belt_clamp_y, carriage_block_depth])
        belt_clamp();

        translate([-belt_clamp_width -0.5*belt_clamp_end_gap,+belt_clamp_y, carriage_block_depth])
        belt_clamp();

}

module x_bearing_block()
{
    linear_extrude(bearing_block_depth+bearing_block_extra)
    hull() {
        translate([carriage_width / 2 - bearing_block_corner_radius, bearing_block_height / 2 - bearing_block_corner_radius, 0])
            circle(r = bearing_block_corner_radius);
        translate([-carriage_width / 2 + bearing_block_corner_radius, bearing_block_height / 2 - bearing_block_corner_radius, 0])
            circle(r = bearing_block_corner_radius);
        translate([carriage_width / 2 - bearing_block_corner_radius, -bearing_block_height / 2 + bearing_block_corner_radius, 0])
            circle(r = bearing_block_corner_radius);
        translate([-carriage_width / 2 + bearing_block_corner_radius, -bearing_block_height / 2 + bearing_block_corner_radius, 0])
            circle(r = bearing_block_corner_radius);
    }
}

module belt_clamp()
{
    points = [
        [0, 0, 0],
        [0, belt_clamp_height - belt_clamp_chamfer, 0],
        [0, belt_clamp_height - belt_clamp_chamfer, belt_clamp_depth],
        [0, belt_clamp_chamfer, belt_clamp_depth],
        [0, 0, belt_clamp_depth - belt_clamp_chamfer],
        [belt_clamp_chamfer, belt_clamp_height, belt_clamp_depth],
        [belt_clamp_chamfer, belt_clamp_height, 0],

        [belt_clamp_width, 0, 0],
        [belt_clamp_width, belt_clamp_height - belt_clamp_chamfer, 0],
        [belt_clamp_width, belt_clamp_height - belt_clamp_chamfer, belt_clamp_depth],
        [belt_clamp_width, belt_clamp_chamfer, belt_clamp_depth],
        [belt_clamp_width, 0, belt_clamp_depth - belt_clamp_chamfer],
        [belt_clamp_width - belt_clamp_chamfer, belt_clamp_height, belt_clamp_depth],
        [belt_clamp_width - belt_clamp_chamfer, belt_clamp_height, 0],

    ];

    faces = [
        [0,1,2,3,4],
        [1,6,5,2],
        [7,11,10,9,8],
        [8,9,12,13],
        [6,13,12,5],
        [5,12,9,10,3,2],
        [3,10,11,4],
        [0,4,11,7],
        [0,7,8,13,6,1],
    ];
    mirror([0,1,0]){

    difference() {
        polyhedron(points = points, faces = faces);
        belt_teeth();
    }

   //#translate([0,0,3]) cylinder(d=1,h=6);

   // plate  to centre load onto axes
    translate([0,-belt_clamp_gap-belt_clamp_plate_thick,0]) {
        hull(){
          cube([belt_clamp_width,belt_clamp_plate_thick,belt_clamp_depth-2]);
          translate([0,-5,0]) cube([belt_clamp_width,belt_clamp_plate_thick+5,belt_clamp_depth-4]);
        }
    }
    translate([0,-10,0]) cube([belt_clamp_width,30,2.0]);
    translate([0,-15,0]) cube([belt_clamp_width,18,2.5]);
}

}

module belt_teeth()
{
    for (i = [belt_tooth_diameter:belt_pitch:belt_clamp_width + belt_tooth_diameter]) {
        translate([i-belt_tooth_diameter, 0, 0])
        hull(){
            cylinder(d = belt_tooth_diameter, h = belt_clamp_depth);
            translate([0,belt_tooth_depth- belt_tooth_diameter, 0])cylinder(d = belt_tooth_diameter, h = belt_clamp_depth);
        }
    }
}

module x_bolt_holes()
{
    translate([-bolt_x_centres / 2, -bolt_y_centres / 2, -5])
        cylinder(d = bolt_hole_dia, h = carriage_block_depth + bearing_block_depth+15);
    translate([-bolt_x_centres / 2, bolt_y_centres / 2,  -5])
        cylinder(d = bolt_hole_dia, h = carriage_block_depth + bearing_block_depth+15);
    translate([bolt_x_centres / 2, -bolt_y_centres / 2,  -5])
        cylinder(d = bolt_hole_dia, h = carriage_block_depth + bearing_block_depth+15);
    translate([bolt_x_centres / 2, bolt_y_centres / 2,  -5])
        cylinder(d = bolt_hole_dia, h = carriage_block_depth + bearing_block_depth+15);
}

module x_bolt_blocks()
{
     block_dia = bolt_block_dia;
     standoff  = bolt_hole_z;
    //4.5
    translate([-bolt_x_centres / 2, -bolt_y_centres / 2, standoff])
        hull(){
           translate([-4,0,0]) cylinder(d =  block_dia,h=+bearing_block_depth);
           translate([-1,0,0]) cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth+2);
         //  cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth);
           translate([1.5,0,0]) cylinder(d =block_dia,h=carriage_block_depth + bearing_block_depth+2);
        }
    translate([-bolt_x_centres / 2, bolt_y_centres / 2, standoff])
        hull(){
           translate([-4,0,0]) cylinder(d =  block_dia,h=+bearing_block_depth);
          translate([-1,0,0]) cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth+2);
         //  cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth);
           translate([1.5,0,0]) cylinder(d =block_dia,h=carriage_block_depth + bearing_block_depth+2);
        }
    translate([bolt_x_centres / 2, -bolt_y_centres / 2, standoff])
        hull(){
           translate([-1.5,0,0]) cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth+2);
          // cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth);
           translate([1,0,0]) cylinder(d =block_dia,h=carriage_block_depth + bearing_block_depth+2);
           translate([ 4,0,0]) cylinder(d =  block_dia,h=+bearing_block_depth);
        }
    translate([bolt_x_centres / 2, bolt_y_centres / 2, standoff])
        hull(){
           translate([-1.5,0,0]) cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth+2);
          // cylinder(d =  block_dia,h=carriage_block_depth + bearing_block_depth);
           translate([1,0,0]) cylinder(d =block_dia,h=carriage_block_depth + bearing_block_depth+2);
           translate([ 4,0,0]) cylinder(d =  block_dia,h=+bearing_block_depth);
        }
}

module x_bearing_cutouts()
{
    if (axle_bearings == 2) x_twin_bearing_cutouts();
    else x_single_bearing_cutouts();
}

module x_single_bearing_cutouts()
{
    translate([-axis_bearing_length / 2, axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_dia, h = axis_bearing_length);

    translate([-axis_bearing_length / 2, -axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_dia, h = axis_bearing_length);

    translate([-carriage_width / 2-1, axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_collar_dia, h = carriage_width+2);

    translate([-carriage_width / 2, -axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_collar_dia, h = carriage_width);

    if ( tiewraps ){

    translate([-axis_bearing_length / 2 + tiewrap_inset - 0.5*tiewrap_width, -axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([axis_bearing_length / 2 - tiewrap_inset + 0.5*tiewrap_width, -axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([0, -axis_bar_centres/2, 0])
    x_tiewrap_cutout();

    translate([-axis_bearing_length / 2 + tiewrap_inset - 0.5*tiewrap_width, axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([axis_bearing_length / 2 - tiewrap_inset + 0.5*tiewrap_width, axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([0, axis_bar_centres/2, 0])
    x_tiewrap_cutout();

    }

}

module x_twin_bearing_cutouts()
{

    translate([-twin_bearing_length - twin_bearing_offset, axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_dia, h = twin_bearing_length);

    translate([twin_bearing_offset, axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_dia, h = twin_bearing_length);

    translate([-twin_bearing_length - twin_bearing_offset, -axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_dia, h = twin_bearing_length);

    translate([twin_bearing_offset, -axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_dia, h = twin_bearing_length);

    translate([-carriage_width / 2-1, axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_collar_dia, h = carriage_width+2);

    translate([-carriage_width / 2-1, -axis_bar_centres/2, axis_bearing_z])
    rotate([0,90,0])
    cylinder(d = axis_bearing_collar_dia, h = carriage_width+2);

    if ( tiewraps ){
    translate([-twin_bearing_length - twin_bearing_offset + tiewrap_inset - 0.5*tiewrap_width, -axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([-twin_bearing_offset - tiewrap_inset + 0.5*tiewrap_width, -axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([twin_bearing_length + twin_bearing_offset - tiewrap_inset + 0.5*tiewrap_width, -axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([twin_bearing_offset + tiewrap_inset - 0.5*tiewrap_width, -axis_bar_centres/2, 0])
    x_tiewrap_cutout();

    translate([-twin_bearing_length - twin_bearing_offset + tiewrap_inset - 0.5*tiewrap_width, axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([-twin_bearing_offset - tiewrap_inset + 0.5*tiewrap_width, axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([twin_bearing_length + twin_bearing_offset - tiewrap_inset + 0.5*tiewrap_width, axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    translate([twin_bearing_offset + tiewrap_inset - 0.5*tiewrap_width, axis_bar_centres/2, 0])
    x_tiewrap_cutout();
    }
}

module x_tiewrap_cutout()
{
    difference() {
        translate([0,0,total_depth / 2-0.1])
        cube(size = [tiewrap_width, tiewrap_inner_height + tiewrap_depth * 2,total_depth+2.5], center = true);

        translate([0,0,tiewrap_bottom_offset+0.5])
            rotate([0, -90, 0])
            minkowski()
            {
                translate([total_depth / 2 + tiewrap_radius, 0, -1])
                cube([total_depth, tiewrap_inner_height - tiewrap_radius * 2, tiewrap_width - 2], center = true);
                cylinder(r=tiewrap_radius,h=2);
            }
    }
}
