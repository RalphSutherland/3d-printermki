//======================================================================
// Uses
//======================================================================
use  <std/fasteners.scad>;
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================
include <../const.scad>
//======================================================================

_height = 86;_thick= 3.0;_width= 72;

plate();

// Mounting hole diameters and spacing
bolt_hole_dia  = 3.7;//3.6 M3 loose fit
bolt_y_centres = _carriageBoltSpacing; //24.0;//23
bolt_x_centres = _carriageBoltSpacing; //24.0;//23

module bolt_holes()
{
    translate([-bolt_x_centres / 2, -bolt_y_centres / 2, -5])
        cylinder(d = bolt_hole_dia, h = 40);
    translate([-bolt_x_centres / 2, bolt_y_centres / 2,  -5])
        cylinder(d = bolt_hole_dia, h = 40);
    translate([bolt_x_centres / 2, -bolt_y_centres / 2,  -5])
        cylinder(d = bolt_hole_dia, h = 40);
    translate([bolt_x_centres / 2, bolt_y_centres / 2,  -5])
        cylinder(d = bolt_hole_dia, h = 40);
}

module plate(){
    color("lightgray")
translate([0,(_thick*0.5)+0.5,_height*0.5]) rotate([90,0,0]){
    
 difference(){
   cube([_width,_height,_thick],true);

   for(i=[[ 4.5,-0.5*_height+20,-5],[-4.5,-0.5*_height+20,-5],[0,-0.5*_height+10,-5]]) translate(i) cylinder(d=1.25,h=10);

    translate([0,0.5*(68 -_height),-30]) bolt_holes();

    
       translate([_width/2-5,-_height/2+6,-_thick-1])
        cylinder(d = bolt_hole_dia, h = 40);
       translate([_width/2-5,-_height/2+21,-_thick-1])
        cylinder(d = bolt_hole_dia, h = 40);
     }

/*
     difference(){
         cube([_width,_height,_thick],true);
         cube([_width-4,_height-4,_thick+2],true);
    }
    */
}

}
//translate([0,-6,0]) e3d();