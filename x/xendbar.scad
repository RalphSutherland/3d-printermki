//======================================================================
// Uses
//======================================================================
use <std/fasteners.scad>
use <std/supports.scad>
use <std/knobs.scad>
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================
include <../const.scad>
//======================================================================

_msize = 4;
// in const _slotoffsetz = -4.0;
_base = 4.5;//mm
echo("axis_bar_centres",_xrodZsep);
echo("offset",_slotoffsetz);

//xendbar(rods= _xrodZsep);
//translate([-25,0,2.5])  knob(d = 21, depth=2, n=24, h=5, hole=4.0);

 xendbarprint(rods=_xrodZsep);

// xendbarinplace(rods= _xrodZsep);
// intersection(){
// xendbarprint(rods= _xrodZsep);
// translate([0,0,0]) cube([6,50,12],center=true);
// }


 module xendbar(rods =_xrodZsep, slot = 25.0){

difference(){

bar(dxb= 17.0,dyb=60,dzb= 7.5+_base,rb=8);
translate([0,0,7+_base]) cube([9,64,12],center=true);
translate([0,-15,11.5+_base]) rotate([ 20,0,0])cube([20,36,8],center=true);
translate([0, 15,11.5+_base]) rotate([-20,0,0])cube([20,36,8],center=true);

translate([0, _slotoffsetz, _base]) cylinder(d=23,h=_base+1);
translate([0, _slotoffsetz, _base+5.5/2]) cube([21.5,21.5,_base+1],center=true);

translate([ 0, -rods/2, -1]) {
                    nutHole(_msize,tolerance = _tol);//cylinder(d=8,h=3,$fn=6);
                    boltHole(_msize,length=20,tolerance = _tol);//cylinder(d=4.1,h=20);
translate([0, 0, 6])washerHole(_msize,length=10);
}

translate([ 0,  rods/2, -1]) {
                    nutHole(_msize,tolerance = _tol);//cylinder(d=8,h=3,$fn=6);
                    boltHole(_msize,length=20,tolerance = _tol);//cylinder(d=4.1,h=20);
translate([0, 0, 6])washerHole(_msize,length=10);
}

}// diff

}


 module xendbarprint(rods =_xrodZsep, slot = 25.0){

translate([ 20,0,  0])   bar(dxb= 7.5, dyb=slot-1);
translate([-25,0,2.5])  knob(d = 21, depth=2, n=24, h=5, hole=4.0);

xendbar(rods =rods, slot = slot);

translate([-8.5, -19.5/2+_slotoffsetz, _base]) supports([4,19.5,5.5],dir=1);
translate([ 4.5, -19.5/2+_slotoffsetz, _base]) supports([4,19.5,5.5],dir=1);

// hole supports
translate([ 0, -rods/2, 0]) {
                    cylinder(d=6.0,h=2.2);
}
translate([ 0,  rods/2, 0]) {
                    cylinder(d=6.0,h=2.2);
}

}


module xendbarinplace(rods =_xrodZsep, slot = 25.0){

color([0.2,0.2,0.2,1.0]) {translate([0,0,-_base]) bar(dyb=slot-2);}

color("Red") {translate([0,+_slotoffsetz,_base+2.5+0.25])  knob(d = 21, depth=2, n=24, h=5, hole=4.25);}

color([0.2,0.2,0.2,1.0]) {xendbar(rods =rods, slot = slot);}

}


module bar( dzb= 3.0,
            dyb= 25.0,
            dxb= 7.5,
            rb=    2,
            hb=    1 ){

     translate([-dxb/2,0,0]){

     difference(){

     hull(){
     minkowski(){
        translate([dxb/2,0,(dzb-hb-0.5)/2])cube([dxb-rb,dyb-rb,dzb-hb-0.5],center=true);
        cylinder(d=rb,h=hb);
     }

     minkowski(){
        translate([dxb/2,0,(dzb-hb)/2])cube([dxb-rb,dyb-rb,dzb-hb],center=true);
        cylinder(d=rb-1,h=hb);
     }
     }

      translate([dxb/2,_slotoffsetz,-1])  boltHole(_msize,length=dzb+2,tolerance = _tol);// cylinder(d=4.25,h=dzb+2);

     }

     }

 }
