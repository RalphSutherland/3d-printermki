//======================================================================
//
// rear mounted 50mm blower mount
// v2.0.3
//
//======================================================================
// Uses
//======================================================================
use <std/boxes.scad>;
use <std/fasteners.scad>;
use <std/supports.scad>;
use <std/fans.scad>;
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================
include <../const.scad>
//======================================================================

// Define curve smoothing (50 is good, anywhere between 20 and 100 will work with 100 being the smoothest)

bolt_y_centres = _carriageBoltSpacing;//24std,24.25,23
bolt_x_centres = _carriageBoltSpacing;//24std,24.25,23
_extra_length  =9.0; // 0.0 E3Dv6 or Chimera, 9.0 3EDv5 or chx jhead
_ShowBlower    = 1;
//======================================================================
// main program, comment components as needed
//======================================================================
//x_blower_duct_print();
x_blower_duct();
//translate([23,10,-9])cube([2,45,2],center=true);
//======================================================================
// modules
//======================================================================


module blower_duct(radius){
translate([0,_extra_length,0]){
color("white")

    scale([1,1,1])
    translate([0,0,-1])
    difference(){
      outerduct(radius);
      innerduct(radius);
// inlet
      translate([13.5+0.05,24.25,0.5])
         cube([27+0.1,3.0,radius+1],center=true);
   } //d

//shaped suports

   rotate([0,180,0]){
   rotate([0,90,0])
      intersection(){
        translate([6, 14.0,-26]){
        rotate([0,0,54]){
            supports(c=[20,40,52]);
         }
         }
         scale([0.95,1,1])
         translate([-1,0,0])
          rotate([0,90,0]) {
              # innerduct(radius);
             }
         }
     }

  translate([0,39.5,-19.])  rotate([43.53,0,0]) cube([3,12,2],center=true);
 }
}

module blower_bolt_holes(dx=24,dy=24)
{
  msize= 3;
 _thick=4;
    translate([-dx / 2, -dy / 2, -5]){
        tapHole(size=msize, length = 20, fit = 1, $fn=6,tolerance = +0.1);
        translate([0,0,0.5*_thick+2.6])nutHole(size=msize,tolerance = +0.1);
        translate([0,0,0.5*_thick+1.25])nutHole(size=msize,tolerance = +0.1);
    }

    hull(){
    translate([-dx / 2+7, dy / 2,  -5]){
        tapHole(size=msize+2, length = 20, fit = 1, $fn=6,tolerance = +0.1);
    }
    translate([dx / 2-7, dy / 2,  -5]){
        tapHole(size=msize+2, length = 20, fit = 1, $fn=6,tolerance = +0.1);
    }
    }

    translate([-dx / 2, dy / 2,  -5]){
        tapHole(size=msize, length = 20, fit = 1, $fn=6,tolerance = +0.1);
        translate([0,0,0.5*_thick+2.6])nutHole(size=msize,tolerance = +0.2);
        translate([0,0,0.5*_thick+1.25])nutHole(size=msize,tolerance = +0.2);
    }
    translate([dx / 2, -dy / 2,  -5]){
        tapHole(size=msize, length = 20, fit = 1, $fn=6,tolerance = +0.1);
        translate([0,0,0.5*_thick+2.6])nutHole(size=msize,tolerance = +0.2);
        translate([0,0,0.5*_thick+1.25])nutHole(size=msize,tolerance = +0.2);
    }
    translate([dx / 2, dy / 2,  -5]){
        tapHole(size=msize, length = 20, fit = 1, $fn=6,tolerance = +0.1);
        translate([0,0,0.5*_thick+2.6])nutHole(size=msize,tolerance = +0.2);
        translate([0,0,0.5*_thick+1.25])nutHole(size=msize,tolerance = +0.2);
    }
}

module blower_top_mount(dx=24,dy=24) {
 _thick=4;

color("white")
translate([22,-20,-12]) {


      difference(){

            union(){
            difference(){
                hull(){
                    translate([0,-2,0]) cylinder(d=10,h=_thick,center=true);
                    translate([-2*22,-2,0]) cylinder(d=10,h=_thick,center=true);
                translate([0,_extra_length,0]){
                    translate([-2*22,2*20+0.2,0])  cube([10,12,_thick],center=true);
                    translate([0,2*20+0.2,0])  cube([10,12,_thick],center=true);
                }
                }
                  translate([-22,20+_extra_length,0]) {
                   cylinder(r=18.0,h=22.0,center=true);
                  }
                 }
                 translate([-22-19,20-18+_extra_length,-2])
                  supports(c=[38,36,4],dir=1);
            }

            deltaX= 43.0;
            deltaY= 38.0;
            // http://docs-asia.electrocomponents.com/webdocs/078a/0900766b8078a106.pdf
            // pg 67
            // and 18mm for missing length
             translate([-22,20,0]) {
                translate([0,_extra_length,0]){
               translate([ 23.0,-20+0.5,-11])
                  tapHole(size=4, length = 22, fit = 1, $fn=6,tolerance = +0.1);
               translate([-20.0,18+0.5,-11]) // asymmetric
                  tapHole(size=4, length = 22, fit = 1, $fn=6,tolerance = +0.1);
                }
                translate([0,-8,0]) blower_bolt_holes(dx,dy);
              }

        }

        translate([0,-8,0])
        translate([0,20,-1])
        difference(){
          union(){
          hull(){
              translate([0,(0.5*dy),0]) cylinder(d=8,h=_thick+2,center=true);
              translate([-2*22,(0.5*dy),0]) cylinder(d=8,h=_thick+2,center=true);
          }
          hull(){
              translate([-8,(0.5*dy)-dy,0]) cylinder(d=8,h=_thick+2,center=true);
              translate([-2*22+8,(0.5*dy)-dy,0]) cylinder(d=8,h=_thick+2,center=true);
          }
          }

          translate([-22,0,1]) {
             blower_bolt_holes(dx,dy);
          }
      }
}


}

module outerduct(radius=20){
      hull(){
      translate([27, 24,-10.5])
      rotate([0,-90,0]){
           rotate_extrude (angle=7,convexity = 4) {//70,60,40
            square([radius+2,54]);
           }//rotate
      }//translate

      translate([0,34,-9]){
      rotate([105,0,0]){
       translate([0,-10,-2.7]) rotate([30,0,0])
          cube([54,3.0,13.0],center=true);
         }//rotate
      }//translate
      }//hull
}

module innerduct(radius=20){
       color("orange")
       hull(){
           translate([27.5,24.7,-9.])
           rotate([0,-90,0]){
                translate([1,1,2])rotate_extrude (angle=5,convexity = 1) {//70
               square([radius-3,51]);
            }
       }

       translate([0,34.4,-8]){
       rotate([105,0,0]){
           translate([0,-10,-1.5]) rotate([30,0,0])
               cube([50,6.0,10.0],center=true);
        }//rot
       }//trans
      }//hull
}

module duct_print(){

rotate([0,180,0]){
rotate([0, 90,0]){
        blower_top_mount(bolt_x_centres,bolt_y_centres);
        blower_duct(21);
        }
     }
}

module x_blower_duct(size=21){
        blower_top_mount(bolt_x_centres,bolt_y_centres);
       if (_ShowBlower>0) translate([0,_extra_length,0])    blower_50mm();
        blower_duct(radius=size);
}
