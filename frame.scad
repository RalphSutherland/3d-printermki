//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/fasteners.scad>
use <std/t-slot.scad>

use <extruder.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

//======================================================================
// main program, comment components as needed
//======================================================================

liteFrame();
feet();
// foot();
//cornerSpacer(_prof,58.5);

//======================================================================
// main program, comment components as needed
//======================================================================

module filamentExtruder(){
// extruders
translate([ -l/2+_prof-5,-_yoff-_prof-70,hg-2.8-_prof]) rotate([180,0,0]) extruder();
//translate([ -l/2+_prof-5,-_yoff-_prof-140,hg-2.8-_prof]) rotate([180,0,0]) extruder();
}


module baseFrame(){
union(){
// sides
translate([-l/2+_h,0,-_h]) rotate([90,0,0])Profile(height=w,size=_prof);
translate([ l/2-_h,0,-_h]) rotate([90,0,0])Profile(height=w,size=_prof);
// ends
translate([0,-w/2+_h-_prof,-_h]) rotate([0,90,0])Profile(height=l,size=_prof);
translate([0, w/2-_h+_prof,-_h]) rotate([0,90,0])Profile(height=l,size=_prof);
//bottom bar
//translate([0,-_yoff-_h,-_h]) rotate([0,90,0])Profile(height=l-2*_prof,size=_prof);

// base corners
translate([-l/2+_prof,-w/2,-_h])  rotate([90,0,90]) cornerBrace(_prof);
translate([-l/2+_prof, w/2,-_h])  rotate([-90,0,-90]) cornerBrace(_prof);
translate([l/2-_prof,-w/2,-_h])  rotate([-90,0,90]) cornerBrace(_prof);
translate([l/2-_prof,w/2,-_h])  rotate([90,0,-90]) cornerBrace(_prof);
}
}

module topFrame(){
union(){
// sides
translate([-l/2+_h,0,-_h]) rotate([90,0,0])Profile(height=w,size=_prof);
translate([ l/2-_h,0,-_h]) rotate([90,0,0])Profile(height=w,size=_prof);

// ends
translate([0,-w/2+_h-_prof,-_h]) rotate([0,90,0])Profile(height=l,size=_prof);
translate([0, w/2-_h+_prof,-_h]) rotate([0,90,0])Profile(height=l,size=_prof);

//top bar
translate([0,-_yoff-_h,-_h]) rotate([0,90,0])Profile(height=l-2*_prof,size=_prof);

// corners
translate([-l/2+_prof,-w/2, -_h]) rotate([ 90, 0, 90]) cornerBrace(_prof);
translate([ l/2-_prof,-w/2, -_h]) rotate([-90, 0, 90]) cornerBrace(_prof);

translate([-l/2+_prof, w/2, -_h]) rotate([-90, 0,-90]) cornerBrace(_prof);
translate([ l/2-_prof, w/2, -_h]) rotate([ 90, 0,-90]) cornerBrace(_prof);

/* mid
#translate([-l/2+_prof,-_yoff-_prof, -_h]) rotate([-90, 0,-90]) cornerBrace(_prof);
#translate([ l/2-_prof,-_yoff-_prof, -_h]) rotate([ 90, 0,-90]) cornerBrace(_prof);
*/
    }

}

module frame(){

baseframe();
translate([0,0,hg+_prof]) topframe();

//mid verts
translate([-l/2+_h,-_yoff-_h,hg/2]) 
    Profile(height=hg,size=_prof);
translate([ l/2-_h,-_yoff-_h,hg/2]) 
    Profile(height=hg,size=_prof);
//back verts
translate([-l/2+_h,-w/2+_h-_prof,hg/2]) 
    Profile(height=hg,size=_prof);
translate([ l/2-_h,-w/2+_h-_prof,hg/2]) 
    Profile(height=hg,size=_prof);
//front verts
translate([-l/2+_h,+w/2-_h+_prof,hg/2]) 
    Profile(height=hg,size=_prof);
translate([ l/2-_h,+w/2-_h+_prof,hg/2]) 
    Profile(height=hg,size=_prof);

translate([-l/2+_h,-w/2, hg])
rotate([ 180,0,90])
barBrace(size=30,l=(-_yoff+w/2-_prof));

translate([ l/2-_h,-w/2, hg])
rotate([ 180,0,90])
barBrace(size=30,l=(-_yoff+w/2-_prof));

translate([-l/2+_h,-w/2, 0])
rotate([  0,0,90])
barBrace(size=30,l=(-_yoff+w/2-_prof));

translate([ l/2-_h,-w/2, 0])
rotate([  0,0,90])
barBrace(size=30,l=(-_yoff+w/2-_prof));

// lower side front
translate([-l/2+_h, w/2, 0])  rotate([ 0,0, -90]) cornerBrace(size=_prof);
translate([ l/2-_h, w/2, 0])  rotate([ 0,0, -90]) cornerBrace(size=_prof);

// upper side front
translate([-l/2+_h, w/2, hg])  rotate([ 180,0, -90]) cornerBrace(size=_prof);
translate([ l/2-_h, w/2, hg])  rotate([ 180,0, -90]) cornerBrace(size=_prof);

// upper front
translate([-l/2+_prof, w/2+_h, hg])  rotate([ 180,0, 0]) cornerBrace(size=_prof);
translate([ l/2-_prof, w/2+_h, hg])  rotate([ 180,0, 180]) cornerBrace(size=_prof);

// upper back
translate([-l/2+_prof, -w/2-_h, hg])  rotate([ 180,0, 0]) cornerBrace(size=_prof);//
translate([ l/2-_prof, -w/2-_h, hg])  rotate([ 180,0, 180]) cornerBrace(size=_prof);

/* upper mid
translate([-l/2+_prof, -_yoff-_h, hg])  rotate([ 180,0, 0]) cornerBrace(size=_prof);
translate([ l/2-_prof, -_yoff-_h, hg])  rotate([ 180,0, 180]) cornerBrace(size=_prof);
*/
// braces to control rod sep
    translate([-(_insideLength/2), -w/2-_h,0]) semibarBrace(size=_prof,l=(_insideLength-_yrodXsep-42)/2);

    translate([ (_insideLength/2), -w/2-_h,0]) rotate([0,0,180])semibarBrace(size=_prof,l=(_insideLength-_yrodXsep-42)/2);

    translate([-(_insideLength/2), w/2+_h,0]) semibarBrace(size=_prof,l=(_insideLength-_yrodXsep-42)/2);

    translate([ (_insideLength/2), w/2+_h,0]) rotate([0,0,180])semibarBrace(size=_prof,l=(_insideLength-_yrodXsep-42)/2);


}

module cornerSpacer(){
    union(){
            translate([-_prof/2,0,_prof+_base])
                    scale([1,1,0.6])
                        spacer(_prof,58.5);
            translate([0,-_prof/2,_prof+_base])
                   scale([1,1,0.6])
                        rotate([0,0,90]) spacer(_prof,58.5);
    }
}

module midFoot(){
    color("Red")
            difference(){
            hull(){
                cylinder(r=26,h=_base);
                translate([0,0,_base])cylinder(r=33.5,h=_prof+7);
            }
            translate([-_prof+_h,-40,_base])cube([2*_prof,80,60]);
            translate([+_h,-40,-1])cube([2*_prof,80,60]);
            }
}

module foot(){
    $fn=60;
       color("Red"){
            difference(){
            hull(){
                                  cylinder(r=40,h=_base);
            translate([0,0,_base])cylinder(r=_prof,h=_prof+6);
            }
            translate([-_prof+_h,-60+_h,_base])cube([3*_prof,3*_prof,60]);

            // base bolts
            /*
            translate([0,0,-1]) {
                boltHole(size=6,length=2*_prof,tolerance = _tol);
                washer(size=6,length=_base-6,tolerance = _tol);
             }
             */
            translate([_prof,0,-1]) {
                boltHole(size=6,length=2*_prof,tolerance = _tol);
                washerHole(size=6,length=_base-6,tolerance = _tol);
             }
            translate([0,-_prof,-1]) {
                boltHole(size=6,length=2*_prof,tolerance = _tol);
                washerHole(size=6,length=_base-6,tolerance = _tol);
            }

            }
        }
}

module feet(standoffy = 30.0){

    translate([ 0,+w/2-_h+_prof,-_prof-_base]) {
        translate([-l/2+_h,0,0]) {
          rotate([0,0,-90])  cornerSpacer();
         foot();
        }
        translate([ l/2-_h,0,0]) {
            rotate([0,0, 180]) cornerSpacer();
            rotate([0,0,-90])foot();
        }
    }

    translate([ 0,-w/2+_h-_prof,-_prof-_base]) {
        translate([-l/2+_h,0,0]) {
             rotate([0,0,0])  cornerSpacer();
         rotate([0,0,90])foot();
        }
        translate([ l/2-_h,0,0]) {
            rotate([0,0,90]) cornerSpacer();
          rotate([0,0,180])foot();
        }
    }
/*
    translate([ 0,standoffy-_yoff,-_prof-_base]) {
        translate([-l/2+_h,0,0]) rotate([0,0,180])midFoot();
        translate([ l/2-_h,0,0]) rotate([0,0,  0])midFoot();
    }
*/

}


module liteFrame(){

baseFrame();

dz = 14 ;// depth below bottom frame
//mid verts
translate([-l/2-_h,-_yoff-_h,(hg+(_prof+dz))/2-_prof-dz]) 
        Profile(height=hg+(_prof+dz),size=_prof);
translate([ l/2+_h,-_yoff-_h,(hg+(_prof+dz))/2-_prof-dz]) 
        Profile(height=hg+(_prof+dz),size=_prof);


//top bar
translate([0,-_yoff-_h,hg+_prof/2]) rotate([0,90,0])
    Profile(height=_topWidth,size=_prof);
translate([l/2+_prof,-_yoff-_h,hg])  rotate([ 0,90,0]) 
    cornerBrace(size=_prof);
translate([-l/2-_prof,-_yoff-_h,hg])  rotate([ 0, 180,0]) 
    cornerBrace(size=_prof);
    
}
