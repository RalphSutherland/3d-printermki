$fn=60;

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

_pi=3.14159265358979;

_tol  = 0.1;    // tight 0.1 for well calibrated printer, may go to 0.15 or even 0.2 if needed
_prof = 20     ;// profile size mm
_profile   = 20; // synonym
_h    = _prof/2;
_mod  =    42.3; // motor side length, mm
_moc  =  _mod/2; // motor center, mm
_modx =    31.0; // motor bolt spacing, mm
_base =   20.0 ; // mm 20 for prof = 20

_xrods  = 420.0; //mm
_xdiam  = 8.0 ; //mm rod diameter
_yrods  = 420.0; //mm
_ydiam  = 8.0 ; //mm rod diameter
_zrods  = 320.0; //mm
_zleads = 300.0; // leadscrew length
_zdiam  = 8.0 ; //mm rod diameter

_platesize = 250.0;  //mm

_yoff   = 30; // mm offset of support
_yoffxz = 27; // mm offset of x-z axis components
_insideLength = _platesize+30; // mm x
_insideWidth  = _yrods-_prof*2; // mm y -
_insideHeight = _zrods-14; // mm z
_topWidth     =    420; // mm
_basespace    =    100; // mm

_zrodoffsetx   = -22.0; //mm
_zrodoffsety   =   6.0; //mm
_xrodoffsetz   =   0.0; // ofset for testing so position of x rods in end mounds by movinh them vertically in the scad model, 0.0 for proper build, 10.0 for testing

_xrodZsep     =    45; //x-rods center-center vertical: max micron 44.5 std: 45
_yrodXsep     =   155; // x-space between y axis rod centers
_rodlr2       =  11.0; // lead screw mount on x mounts cylinder
_rodbr2       =  11.5; // 11.5 LM8 13.5 LM10, bearing mount on x mounts
_rodoffdepth  =  44.5; //0.5*(_xrods-_insideLength)-_prof-_moc-1+_rodbr2;//45.0;//mm into x blocks rel to straight rod mount
_slotoffsetz  =   0.0;// -4.0; // vertical offset for x axis belts between rods std 0 , 44.5:-4
_extensionLength =  0;
_xendLength   =   342; //(_insideLength + 2*_prof)+2*(_moc+1-_rodbr2-_zrodoffsetx);// mm from xend to xend

_carriageBoltSpacing = 24;
_carriage_width      = 56;
_carriage_height     = 68;
_carriage_axis_depth = 7.5+4-1;// see xcarriage.scad axis_bearing_z

// hot end mounting plate backing plate
_hotplate_width  = 69; //E3Dv5 side prox: 69mm, incl 5mm margin on right on 64mm base width;
_hotplate_height = 90; //E3Dv5 std mount: 90mm;
_hotplate_thick  = 3.0;//std aluco signboard 3.0mm;

l  = _insideLength + 2*_prof;
w  = _insideWidth;
hg = _insideHeight;

// Colors!
_footColor = [0.8,0.2,0.2,1.0];
_fitColor  = "White";
_fitClearColor = [1.0,1.0,1.0,0.6];

_xaxisColor  = [0.2,0.2,0.2,1.0];
_yaxisColor  = [0.2,0.2,0.2,1.0];
_zaxisColor  = [0.2,0.2,0.2,1.0];

//======================================================================
