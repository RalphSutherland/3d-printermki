//
// MPX3 Controller Case
// v1.0.1
// RSS2016
//
use <std/boxes.scad>
use <std/fasteners.scad>
//======================================================================
// Parameters
//======================================================================
$fn=60;

_TOL       = 0.25;
_wall      = 3.0;
_corner    = 3.0;
_lip       = 0.5*_wall-0.125;
_lipdepth  = 1.5;
_floor     = 3.0;
_insidex   = 100+_TOL*2;
_insidey   = 155+_TOL*2;
_insidez2  =  50+_TOL*2;  // available space when shut
_insidez1  = _insidez2-_lipdepth+_TOL;
_insidebz1 = 8+_TOL*2;  // available space when shut
_insidebz2 = _insidebz1+_lipdepth-_TOL;
//
_mountDx = 76;
_mountDy = 135;
_holeDiam = 4.0;
_supportheight = 5.0;
//
_usbplug_w = 15; // mm
_usbplug_y = 17; // mm

//======================================================================
// main program, comment components as needed
//======================================================================

mpxbox();
//translate([_insidex+2*_wall,0,_insidebz2+_insidez2+_floor+2*_lipdepth])rotate([0,180,0])mpxlid();
//translate([0,0,40]) mpxbox();
//translate([_insidex+2*_wall+5,0,0])
//mpxlid();

//======================================================================
// modules
//======================================================================
//======================================================================
module mpxlid(){
//======================================================================
   difference(){
        union(){
            difference(){
                translate(0.5*[2*_wall + _insidex, 2*_wall + _insidey,_floor + _insidez2 ]) roundedBox([2*_wall + _insidex, 2*_wall + _insidey,_floor + _insidez2 ],_corner,true);
                translate([_lip,_lip,_floor + _insidez1]) {
                    translate(0.5*[_insidex+2*(_wall-_lip),_insidey+2*(_wall-_lip),_insidez2]) roundedBox([_insidex+2*(_wall-_lip),_insidey+2*(_wall-_lip),_insidez2],0.5*_corner,true); // lip
                }
                translate([_wall+0.5*_insidex,_wall+0.5*_insidey,_floor+0.5*_insidez2]) roundedBox([_insidex,_insidey,_insidez2],0.5*_corner,true); // main space
            } // diff

            // corner posts
            translate([2*_wall + _insidex - 3.5, 2*_wall + _insidey - 3.5, _floor]) cylinder(d=6,h=_insidez2);
            translate([2*_wall + _insidex - 3.5,                      3.5, _floor]) cylinder(d=6,h=_insidez2);
            translate([                     3.5, 2*_wall + _insidey - 3.5, _floor]) cylinder(d=6,h=_insidez2);
            translate([                     3.5,                      3.5, _floor]) cylinder(d=6,h=_insidez2);

            // mid posts
            translate([2*_wall + _insidex - 4.25, 0.5*(2*_wall + _insidey), _floor]) cylinder(d=5,h=_insidez2-_lipdepth);
            translate([                     4.25, 0.5*(2*_wall + _insidey), _floor]) cylinder(d=5,h=_insidez2-_lipdepth);

        } // union

       dx = 0.5*(_insidex - _mountDx);
       dy = 0.5*(_insidey - _mountDy);

       translate([_wall+0.5*_insidex,_wall+0.4*_insidex,-0.5]){
           cylinder(d=57,h=_floor+1);
          for(p = [[-25,-25,0],[25,-25,0],[25,25,0],[-25,25,0]]) {
           translate(p)cylinder(d=4.5,h=_floor+1);
          }
       }

//       grille

      hx = 15.5;
      hhx = hx*0.5;
      hy = hx;
      hhy = hy*0.5;

      gridNX = 4;//floor(_insidex/(hx+5));
      gridNY = 2;//floor(_insidex/(hx+5));
      gridDx = gridNX*hx;
      gridDy = gridNY*hx;

      gdx = 0.5*(_insidex - gridDx);
      gdy = 100;//0.5*(_insidey - gridDy);

      for(i = [0 : gridNX]) {
           for(j= [ 0-i%2: gridNY]) {
                     translate([_wall+gdx+0+i*hx,_wall+gdy+j*hy+(i%2)*hhy,-1]) 
                         hexprism(r=hhx-1,h=_floor+2);
               }
      }
        // connector gaps
      // stepper connectors

      translate([1.5*_wall+_insidex, 0, _floor*2])
      for( cz=[0:4]){
      for( c=[0:5]){
          pc = [0,_wall+(_insidey*((0.75+c)/6.5)),cz*10];
          translate(pc) cube([_wall+1,13,6],center=true);
      }
      }

      // power connectors
      translate([0,_wall+0.5, _floor+_insidez1*0.35])
      for( c=[2:3]){
          pc = [_wall+_insidex*(0.25+c)/4,0,0];
          translate(pc) rotate([90,0,0])cylinder(d=16, h=_wall+1);
      }

    // translate([18,0.5*(_wall-5),_floor+_insidez2-15]) cube([14,5,20]);
       translate([_wall+dx+_mountDx*0.5-_TOL,_wall+dy+_TOL-_usbplug_w,_floor+0.5*_usbplug_w+_supportheight+(_insidez2-_usbplug_y)]) cube([_mountDx+10,_usbplug_w,_usbplug_y],center=true);

// display and stops connectors
      translate([0,1.5*_wall+_insidey, 2*_floor])
      for( cz=[0:4]){
          for( c=[0:4]){
              pc = [_wall+(_insidex*((0.75+c)/5.5)),0,cz*10];
              translate(pc) cube([13,_wall+1,6],center=true);
          }
      }

// vents
      translate([0.5*_wall, 0, _floor*2]){
      for( cz=[0:3]){
      for( c=[0:5]){
          pc = [0,_wall+(_insidey*((0.75+c)/6.5)),cz*10];
          translate(pc) cube([_wall+1,13,6],center=true);
      }
      }
      for( c=[2:5]){
          pc = [0,_wall+(_insidey*((0.75+c)/6.5)),4*10];
          translate(pc) cube([_wall+1,13,6],center=true);
      }
      }


     translate([_wall-0.25*_usbplug_w,_wall+dy+_TOL+_usbplug_y+0.5*_usbplug_w+3,_floor+0.5*_usbplug_y+_supportheight+(_insidez2-_usbplug_y)]) cube([_usbplug_w,21,_usbplug_y],center=true);

// through holes
       translate([2*_wall + _insidex  - 3.5, 2*_wall + _insidey -  3.5, 0]) cylinder(d=2.9,h=_floor+_insidez2+1);
       translate([2*_wall + _insidex  - 3.5,                       3.5, 0]) cylinder(d=2.9,h=_floor+_insidez2+1);
       translate([                      3.5, 2*_wall + _insidey -  3.5, 0]) cylinder(d=2.9,h=_floor+_insidez2+1);
       translate([                      3.5,                       3.5, 0]) cylinder(d=2.9,h=_floor+_insidez2+1);

        // mid pin holes
       translate([2*_wall + _insidex  - 4.25, 0.5*(2*_wall + _insidey), 0]) cylinder(d=1.5,h=_floor+_insidez2+1);
       translate([                      4.25, 0.5*(2*_wall + _insidey), 0]) cylinder(d=1.5,h=_floor+_insidez2+1);

    }// diff
}
//======================================================================

//======================================================================
module mpxbox(){
//======================================================================
   difference(){
        union(){
            difference(){
                union(){
                   translate(0.5*[2*_wall + _insidex, 2*_wall + _insidey,_floor + _insidebz2 ]) roundedBox([2*_wall + _insidex, 2*_wall + _insidey,_floor + _insidebz2 ],_corner,true);
                   translate([_wall-_lip,_wall-_lip,_floor + _lipdepth]) {
                       translate(0.5*[_insidex+2*_lip,_insidey+2*_lip,_insidebz2]) roundedBox([_insidex+2*_lip,_insidey+2*_lip,_insidebz2],0.5*_corner,true);
                   }
                }
             translate([_wall+0.5*_insidex,_wall+0.5*_insidey,_floor+0.5*(_insidebz2+_lipdepth)]) roundedBox([_insidex,_insidey,_insidebz2+_lipdepth+_TOL],0.5*_corner,true); // main space
            } // diff

            /// supports
            dx = 0.5*(_insidex - _mountDx);
            dy = 0.5*(_insidey - _mountDy);
            support(_wall+dx+_TOL,_wall+dy+_TOL,_floor);
            support(_wall+dx+_TOL,_wall+dy+_mountDy+_TOL,_floor);
            support(_wall+dx+_mountDx+_TOL,_wall+dy+_mountDy+_TOL,_floor);
            support(_wall+dx+_mountDx+_TOL,_wall+dy+_TOL,_floor);

            // corner posts
            translate([2*_wall + _insidex - 3.5, 2*_wall + _insidey - 3.5, _floor]) cylinder(d=6,h=_insidebz2);
            translate([2*_wall + _insidex - 3.5,                       3.5, _floor]) cylinder(d=6,h=_insidebz2);
            translate([                     3.5, 2*_wall + _insidey - 3.5, _floor]) cylinder(d=6,h=_insidebz2);
            translate([                     3.5,                       3.5, _floor]) cylinder(d=6,h=_insidebz2);

            // mid posts
            translate([2*_wall + _insidex - 4.25, 0.5*(2*_wall + _insidey), _floor]) cylinder(d=5,h=_insidebz2+_lipdepth);
             translate([                    4.25, 0.5*(2*_wall + _insidey), _floor]) cylinder(d=5,h=_insidebz2+_lipdepth);

        } // union

        // support holes and floor holes
           dx = 0.5*(_insidex - _mountDx);
           dy = 0.5*(_insidey - _mountDy);
           supporthole(_wall+dx+_TOL,_wall+dy+_TOL,0);
           supporthole(_wall+dx+_TOL,_wall+dy+_mountDy+_TOL,0);
           supporthole(_wall+dx+_mountDx+_TOL,_wall+dy+_mountDy+_TOL,0);
           supporthole(_wall+dx+_mountDx+_TOL,_wall+dy+_TOL,0);

        // grille

            hx = 15.5;
            hhx = hx*0.5;
            hy = hx;
            hhy = hy*0.5;

            gridNX = floor(_insidex/(hx+5));
            gridNY = floor(_insidey/(hx+5));
            gridDx = gridNX*hx;
            gridDy = gridNY*hx;

            gdx = 0.5*(_insidex - gridDx);
            gdy = 0.5*(_insidey - gridDy);

            for(i = [0 : gridNX]) {
                 for(j= [ 0-i%2: gridNY]) {
                           translate([_wall+gdx+0+i*hx,_wall+gdy+j*hy+(i%2)*hhy,-1]) hexprism(r=hhx-1,h=_floor+2);
                     }
            }

        // connector gaps
           translate([_wall+dx+_mountDx*0.5+_TOL,_wall+dy+_TOL-_usbplug_w,_floor+0.5*_usbplug_w+_supportheight]) cube([_mountDx+10,_usbplug_w,_usbplug_w],center=true);
           translate([_wall+dx+_mountDx+_usbplug_w+_TOL,_wall+dy+_TOL+_usbplug_y+0.5*_usbplug_w,_floor+0.5*_usbplug_w+_supportheight]) cube([_usbplug_w,_usbplug_w,_usbplug_w],center=true);

        // lidstops
           translate([2*_wall + _insidex - 3.5, 2*_wall + _insidey - 3.5, _floor+_insidebz2]) cylinder(d=6.5,h=_lipdepth+1);
           translate([2*_wall + _insidex - 3.5,                      3.5, _floor+_insidebz2]) cylinder(d=6.5,h=_lipdepth+1);
           translate([                     3.5, 2*_wall + _insidey - 3.5, _floor+_insidebz2]) cylinder(d=6.5,h=_lipdepth+1);
           translate([                     3.5,                      3.5, _floor+_insidebz2]) cylinder(d=6.5,h=_lipdepth+1);

        // through holes
           translate([2*_wall + _insidex  - 3.5, 2*_wall + _insidey -  3.5, 0]) cylinder(d=2.9,h=_floor+_insidebz2+1);
           translate([2*_wall + _insidex  - 3.5,                       3.5, 0]) cylinder(d=2.9,h=_floor+_insidebz2+1);
           translate([                      3.5, 2*_wall + _insidey -  3.5, 0]) cylinder(d=2.9,h=_floor+_insidebz2+1);
           translate([                      3.5,                       3.5, 0]) cylinder(d=2.9,h=_floor+_insidebz2+1);

        // mid pin holes
           translate([2*_wall + _insidex  - 4.25, 0.5*(2*_wall + _insidey), 0]) cylinder(d=1.5,h=_floor+_insidebz2+1);
           translate([                      4.25, 0.5*(2*_wall + _insidey), 0]) cylinder(d=1.5,h=_floor+_insidebz2+1);

   }// diff

}

//======================================================================

module hexprism(r,h){
	cylinder(r=r,h=h,$fn=6);
}

function mod(i, j) = round(((abs(i)/abs(j))-floor(abs(i)/abs(j)))*j);

module support(x,y,z){
    pos = [x,y,z];
   translate(pos)  cylinder(r1=0.5*_holeDiam+_wall+_TOL,r2=0.5*_holeDiam+1+_TOL,h= _supportheight);
}

module supporthole(x,y,z){
    pos   = [x,y,z-1.0];
    posn1 = [x,y,z-0.0];
    posn2 = [x,y,z+_TOL];
    posn3 = [x,y,z-_TOL];
    union(){
      translate(pos)  cylinder(r=0.5*_holeDiam+_TOL,h= _floor+_supportheight+2);
      translate(posn1)  nutHole(3,tolerance=0.5);
      translate(posn2)  nutHole(3,tolerance=0.5);
      translate(posn3)  nutHole(3,tolerance=0.5);
  }
}

