/*
 * xcarriage.scad
 * v1.0.0 29th July 2016
 * Written by landie @ Thingiverse (Dave White)
 *
 * This script is licensed under the Creative Commons - Attribution license.
 *
 * http://www.thingiverse.com/thing:1695253
 *
 * The parameters below can be used to customise the bracket
 * I've provided descriptions alongside the key ones
 */
 use <std/boxes.scad>

// Mounting bolt holes - M3 bolts through the print bed base into the top of the clamp
mount_hole_spacing = 40; // space between the hole centres
mount_hole_dia = 3.6; // diameter of the hole for the bolts to pass through
mount_nut_dia = 6.4; // outside diameter of the captive nut cutout
mount_nut_depth = 2; // depth of the nut cutout

// carriage dimensions
carriage_y = 30; // length of the carriage along the y axis
carriage_z = 18; // depth between the bed and the top of the belt
carriage_plate_thickness = 6; // Thickness of the mounting plate at the top of the plate, needs to be quite thick so it's rigid
carriage_side_thickness = 6; // Thickness of the rest of the plates that make up the bracket

// Variables for the two belt clamps
belt_clamp_height = 9.5; //distance between the teeth and the bottom of the clamp
belt_clamp_width = 13; // 23 length of each clamp along the y axis
belt_clamp_chamfer = 2; // size of the chamfers used on the clamps
belt_clamp_depth = 10; // the width of the clamp along the x-axis when fitted - adjust if you want to use a wider belt
belt_tooth_diameter = 1.25; // used for the cutouts in the clamp for the belt teeth - this works well for most belts as is
belt_pitch = 2; // pitch of the teeth in the belt, 2 is normal
belt_gap = 1.1; // the gap between the teeth and the plate

// Calculated values
carriage_x = ((belt_clamp_depth - belt_clamp_chamfer) / 2 + carriage_side_thickness) * 2; // width of the carriage plate
side_offset = (belt_clamp_depth - belt_clamp_chamfer) / 2 + carriage_side_thickness / 2;
side_z = carriage_z - carriage_plate_thickness + belt_clamp_height;
infill_z = carriage_z - carriage_plate_thickness - carriage_side_thickness - belt_gap;

$fn = 50;

yclamp();

function yclampheight()=carriage_z;
function yclampwidth()=carriage_y+20;

module yclamp(){
rotate([0,0,0])
difference() {
    carriage();
    mounting_holes();
}
}

module carriage()
{
    translate([0,0,carriage_z - carriage_plate_thickness / 2]){
    roundedBox([carriage_x, carriage_y+25, carriage_plate_thickness],2, true);
    //#cube([carriage_x, carriage_y+25, carriage_plate_thickness],center= true);
    }
    translate([side_offset, 0, side_z / 2 - belt_clamp_height])
    cube([carriage_side_thickness, carriage_y, side_z], center = true);
    
    // side infills
    translate([0,carriage_y / 2 - carriage_side_thickness / 2,infill_z /2 + belt_gap + carriage_side_thickness])
    cube([belt_clamp_depth, carriage_side_thickness, infill_z], center = true);
    translate([0,-carriage_y / 2 + carriage_side_thickness / 2,infill_z /2 + belt_gap + carriage_side_thickness])
    cube([belt_clamp_depth, carriage_side_thickness, infill_z], center = true);
    
    // belt clamps and block
    translate([0,0,carriage_side_thickness /2 + belt_gap])
    cube([belt_clamp_depth, carriage_y, carriage_side_thickness], center = true);
    translate([0,carriage_y / 2 - belt_clamp_width / 2,0])
    belt_clamp();
    translate([0,-carriage_y / 2 + belt_clamp_width / 2,0])
    belt_clamp();
}

module belt_clamp()
{
    points = [
        [0, 0, 0],
        [0, belt_clamp_height - belt_clamp_chamfer, 0],
        [0, belt_clamp_height - belt_clamp_chamfer, belt_clamp_depth],
        [0, belt_clamp_chamfer, belt_clamp_depth],
        [0, 0, belt_clamp_depth - belt_clamp_chamfer],
        [belt_clamp_chamfer, belt_clamp_height, belt_clamp_depth],
        [belt_clamp_chamfer, belt_clamp_height, 0],
        
        [belt_clamp_width, 0, 0],
        [belt_clamp_width, belt_clamp_height - belt_clamp_chamfer, 0],
        [belt_clamp_width, belt_clamp_height - belt_clamp_chamfer, belt_clamp_depth],
        [belt_clamp_width, belt_clamp_chamfer, belt_clamp_depth],
        [belt_clamp_width, 0, belt_clamp_depth - belt_clamp_chamfer],
        [belt_clamp_width - belt_clamp_chamfer, belt_clamp_height, belt_clamp_depth],
        [belt_clamp_width - belt_clamp_chamfer, belt_clamp_height, 0],
        
    ];
    
    faces = [
        [0,1,2,3,4],
        [1,6,5,2],
        [7,11,10,9,8],
        [8,9,12,13],
        [6,13,12,5],
        [5,12,9,10,3,2],
        [3,10,11,4],
        [0,4,11,7],
        [0,7,8,13,6,1],
    ];
    
    rotate([-90,0,90])
    translate([-belt_clamp_width / 2, 0, -(belt_clamp_depth - belt_clamp_chamfer) / 2])
    difference() {
        polyhedron(points = points, faces = faces);
        belt_teeth();
    }
}

module belt_teeth()
{
    for (i = [belt_tooth_diameter:belt_pitch:belt_clamp_width + belt_tooth_diameter]) {
        translate([i, 0, 0])
            cylinder(d = belt_tooth_diameter, h = belt_clamp_depth);
    }
}


module mounting_holes()
{
    translate([0,mount_hole_spacing / 2, carriage_z])
    mounting_hole();
    translate([0,-mount_hole_spacing / 2, carriage_z])
    mounting_hole();
}

module mounting_hole()
{
    translate([0,0,-carriage_plate_thickness-1])
    cylinder(d = mount_hole_dia, h = carriage_plate_thickness+2);
    translate([0,0,-carriage_plate_thickness-1])
    cylinder(d = mount_nut_dia, h = mount_nut_depth+1, $fn = 6);
}