/*
Name: OpenSCAD model of a MaxMicron 20-20 profile Y-Axis Motor Mount

Can generate single piece, stronger, for printing with supports, or
two pieces to print wihtout supports. Edit one_piece as needed.

Printed well with PLA, 5 perimeters, 5 top and bottom layers, 40% fill
and 0.25 layers, 0.4 nozzle and 0.5mm width.

Author: R Sutherland

License: CC BY-SA 4.0
License URL: https://creativecommons.org/licenses/by-sa/4.0/
*/
//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/t-slot.scad>
use <std/fasteners.scad>
use <std/motors.scad>
use <std/GT2Gear.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <../const.scad>

one_piece = true;
_Mbolt = 5;// M5 or M4 holes

_thick = 6;// bracket thickness
_mlen  = 40; // longer motor, std = ~40
_len   = _mlen+48; // longer motor, std = ~48
_vertical = (_prof/2)-(_moc-15.0);//(10-6.15)=3.85
_offy = 15;
echo("_vertical",_vertical);

//
//======================================================================
//

_col = _yaxisColor;

_showMotor     = 0;
_showProfile   = 0;
_showHardware  = 0;

/*
ymotor(length=40);
if (_showProfile == 1) {
    translate([25,-_prof/2,-_prof/2])
    rotate([0,90,0])tslotProfile(size=_prof,length=130);
}
*/

yidle(d=18,gap=9);
if (_showProfile == 1) {
    translate([25,_prof/2,-_prof/2])
    rotate([0,90,0])tslotProfile(size=_prof,length=130);
}

//yrodblock();

module yidle(d=14,h=8,p=_prof,gap=9){

    
 //   translate([0,0,-_prof/2])
    translate([0,-_thick,0]){
        rotate([90,0,0]) {
            yblock(w=70, l=30, h=16, gap=gap,d=d,p=p);
          }

       if (_showHardware == 1) {
           translate([  0,  -_offy, _vertical-_prof/2]) 
           rotate([90,0,90]) {
                color("Silver")GT2Nidle(20, d=18, h=8.5);
            }
        }

    }
 /*    difference(){
        translate([0,20,0]) rotate([90,0,0]) yblock(w=68,h=16,l=36,p=p);
        rotate([0,90,0]){
            translate([ 0, 0,-h/2]) cylinder(d=d+2,h=h);
            translate([ 0,-4,  0]) cube([d+5,d+2,h],center=true);
            translate([ 0, 0,-12.5]) boltHole(3,length=25);
            translate([ 0, 0,-16]) cylinder(d=6.6,h=6);
            translate([ 0, 0, 10]) cylinder(d=6.6,h=6,$fn=6);
        }
    }
    */
 }

module ymotor(length=_mlen){

if (one_piece){
    translate([_thick*2,0,0])
    rotate([0,0,0]){
       motor_mount(length);
       color("Red")profile_mount_full();
    }
} else {
    translate([0,0,_thick])
    rotate([0,90,0]){
        motor_mount(mlen=length);
        color("Red")profile_mount_short();
        color("Red")translate([-24,0,50]) profile_mount_motor();
    }
}

}

//function yrodblockheight (d=8)  = 2*d ;
//function yrodblockbelow (d=8)  = d ;

function yrodblockheight (d=8, t=8)  = d+t ;
function yrodblockwidth  (d=8, t=8)  = 6*d ;
function yrodblockbelow  (d=8, t=8)  = (d + t)/2;


module yrodblock(d=8, m=5, t=8, p=20){

    wid = (p==30)?8:6;
    color(_col)
    difference(){
        union(){
            yrodmount (w=6*d, h=2*d, t=t, d=d, p=p);
            translate([ 0,0, -(t+d)/2-1]){
                hull(){
                     translate([ -d/2, 0, 0]) cylinder(d=wid,h=2);
                     translate([  d/2, 0, 0]) cylinder(d=wid,h=2);
                }
            }
        }
       rotate([90,0,0]) translate([0,0,-(p+2)/2]) cylinder(d= d+_tol*4, h=p+2);

       translate([ 2*d,0,0]) {
          translate([ 0,0,-(t+d)/2-1]) boltHole(size=m,length=d+t+2,tolerance=_tol);
          translate([ 0,0, (t-d)/2-1]) washerHole(size=m,length=d+t,tolerance=_tol);
       }

       translate([-2*d,0,0]) {
          translate([ 0,0,-(t+d)/2-1]) boltHole(size=m,length=d+t+2,tolerance=_tol);
          translate([ 0,0, (t-d)/2-1]) washerHole(size=m,length=d+t,tolerance=_tol);
       }

    }
}

module yrodmount (w=40, h=16, d=8, t=8, p=20) {

    //
    // idle block
    //
     color(_col)hull(){
        translate([ 0, 0,  d/2])  roundedBox([ 1.25*h, p, t], 2, true); 
        translate([ 0, 0, -t/2])  roundedBox([ 2*h, p, t], 2, true);
      }

      translate([0,0, -d/2]) roundedBox([w,p,t],2,true);

}

/*
module yrodblock(d=8, p=20){

    w = 5*d;
    h = 2*d;
    _tol = 0.2;
    difference(){
        yblock (w=w, h=h, p=p);
        rotate([90,0,0]) translate([0,0,-(p+2)/2]) cylinder(d= d+_tol, h=p+2);
    }
}
*/

module yblock (w=40, h=16, l=20, gap=8.5, d=18, p=20) {


    length = 70;
    height = 34;
    xlen = 40.0;

    difference(){
        union(){

    difference(){
        union(){

    // mounting plates
    color(_col) 
    rotate([90,0,180]){

            // backplate
            translate([0,-_thick/2,-(p-height/2)]) rotate([90,0,0]) 
                roundedBox([length,height,_thick,],5,true);//cube([length,_thick,_thick+height]);

            //profile shelf
            translate([0,-p/2-_thick/2,_thick/2]) 
                 roundedBox([length,p+_thick,_thick],5,true);//cube([length,_prof,_thick]);
            translate([0,-p/2-_thick/2,(height-p)/2]) rotate([0,90,0]) 
                 roundedBox([height-p,p+_thick,_thick,],5,true); //cube([_thick,_prof,_vertical-1]);


    }//rotate 
    //
    // idle block
    //
     translate([0,-p/2+_vertical,-_thick+2]){
        h2   = h-4;
       h4   = h+4;
       hl   =  (h<l)?l:h;
    //
         color(_col)
         hull(){
        translate([0,0, (hl-h)/2+_thick]) 
             roundedBox([ 1.5*h,p,hl],2,true); 
        translate([0,0,2]) 
             roundedBox([w-h*2,p,h2/2],2,true);
        }//hull
      } // translate 
      } //union 

     // cutout for idle wheel and axels
           translate([  0,-p/2+_vertical, _offy]) rotate([90,0,90]) 
           union(){
                translate([  0,  0, -gap/2-_tol]) cylinder(d=d+2,h=gap+2*_tol);
                translate([  0, 4,    0]) cube([p+2,d+2,gap+2*_tol],center=true);
            }//union

    //
     } // diff


       translate([  0,-p/2+_vertical, _offy]) rotate([90,0,90]) 
       union(){

          // mount points
          translate([0,0,-(gap+1)/2])rotate([   0,0,0])cylinder(d1=12,d2=10,h=1);
          translate([0,0, (gap+1)/2])rotate([ 180,0,0])cylinder(d1=12,d2=10,h=1);

        }//union

    }

     // bolt holes axle
           translate([  0,-p/2+_vertical, _offy]) 
            rotate([90,0,90]) union(){
               m=3;
                translate([  0,  0,-12.5]) boltHole(size=m,length=25, tolerance=_tol);
                translate([  0,  0,  -18]) washerHole(size=m,length=6, tolerance=_tol);
                //translate([  0,  0,   10]) nutHole(size=m, tolerance=_tol);
                translate([  0,  0,   12]) nutHole(size=m,tolerance=_tol);
                translate([  0,  0,   14]) nutHole(size=m,tolerance=_tol);
            }

     // bolt holes mounting
        translate([0,0,-6]){
                translate([ -60+length-10,-_prof/2,-1]) boltHole(5,length=10,tolerance=_tol);
                translate([ -60+length-10,-_prof/2,5])  cylinder(d=gap+2*_tol,h=10);//cylinder(d=2*_Mbolt,h=30,tolerance=_tol);
        }
       // top
        translate([-20,p/2+_thick,-p/2-_thick])  rotate([90,0,0]) {
            translate([ 0, 0, _thick+3]) washerHole(_Mbolt,length=2,tolerance=_tol);
            translate([ 0, 0, _thick+2]) boltHole(_Mbolt,length=10,tolerance=_tol);
        }

        translate([ 20,p/2+_thick,-p/2-_thick])  rotate([90,0,0]) {
            translate([ 0, 0,  _thick+3]) washerHole(_Mbolt,length=2,tolerance=_tol);
            translate([ 0, 0, _thick+2]) boltHole(_Mbolt,length=10,tolerance=_tol);
        }

        

     }// diff

      if (_showHardware){
           translate([  0,-p/2+ _vertical, 15]) rotate([90,0,90]) {
               color("Silver") translate([  0,  0,-12]) 
                    socketBolt(3,length=25,tolerance=_tol);
            }

     // bolt holes mounting
        translate([0,0,0]){
            translate([ -60+length-10,-p/2,0]) rotate([180,0,0]) 
                    socketBolt(5,length=12,tolerance=_tol);
        }


        translate([-20,_thick-1,-p/2-_thick])  rotate([90,0,0]) {
                 socketBolt(_Mbolt,length=10,tolerance=_tol);
        }
        translate([ 20,_thick-1,-p/2-_thick])  rotate([90,0,0]) {
                  socketBolt(_Mbolt,length=10,tolerance=_tol);
        }


    }

}


/*
module yblock (w=40, h=16, l=20, p=20) {
//
    _tol = 0.2;
    h2   = h-4;
    h4   = h+4;
    hl   = (h<l)?l:h;
//
    difference(){
    union(){
    hull(){
        translate([0,0, (hl-h)/2]) roundedBox([ h,p,hl],2,true); //cube([h,p,h],center=true);
        translate([0,0,-2]) roundedBox([w-h*2,p,h2],2,true);
    }
        translate([0,0,-2]) roundedBox([w,p,h2],2,true);
    }
//
    translate([w/2-8,0,0]){
        translate([0,0,-h2-1])cylinder(d= 5.5, h=2*h+2);
        translate([0,0,0])cylinder(d= 10, h=2*h+2);
    }
    translate([-w/2+8,0,0]){
        translate([0,0,-h2-1])cylinder(d= 5.5, h=2*h2+2);
        translate([0,0,0])cylinder(d= 10, h=2*h2+2);
    }
    }
//
}


*/
module motor_mount(mlen=40){

difference(){
color("Red")
union(){
    translate([3,_moc+_thick,-(_moc-15)]) rotate ([0,90,0]) 
        roundedBox([_mod,_mod,_thick],3,true);
    translate([0,_moc-_thick,-(_moc-15)]) rotate ([0,90,0]) 
       cube([_mod,_moc,_thick*2],center=true);
}
translate([-8,_moc+_thick,-(_moc-15)]) rotate ([0,90,0]) {
    stepper_motor_mount(17,slide_distance=0, mochup=false, tolerance=0.1);
}
translate([-8,_moc+_thick,-(_moc-15)]) rotate ([0,90,0])  
    cylinder(d=23,h=20); // slightly larger hole for some steppers

}

translate([mlen+6,_moc+_thick,-(_moc-15)]) rotate ([90,90,-90]) {
    stepper_motor(17,length=mlen);
    color("Silver")translate([0,0,mlen+18]) GT2N(20);
}

}

module profile_mount_full(){
difference(){
color("blue")
union(){
translate([-40,0,15.0-_mod]) cube([70.0,_thick,_mod]);
translate([-40,-20,0]) cube([70,20,_thick]);
translate([-20,7.5,_thick/2]) roundedBox([40,15,_thick],5,true);
translate([-10,7.5,_thick/2]) cube([20,15,_thick],center=true);
translate([-20,7.5,-_moc+_thick]) roundedBox([40,15,_thick],5,true);
translate([-10,7.5,-_moc+_thick]) cube([20,15,_thick],center=true);
}
hull(){
translate([ 20,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
translate([ 22,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
}
hull(){
translate([  -4,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
translate([  -6,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
}
hull(){
translate([-30,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
translate([-32,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
}
color("yellow")  translate([ -30,16,-10])  rotate([90,0,0]) boltHole(8,length=10,tolerance=0.1);
color("yellow")  translate([ -30,7,-10])  rotate([90,0,0]) boltHole(_Mbolt,length=10,tolerance=0.1);
}

}

module profile_mount_short(){
difference(){
color("blue")
union(){
translate([-40,0,-27.2]) cube([46.0,_thick,_mod]);
translate([-40,-20,0]) cube([46,20,_thick]);
translate([-20,7.5,3]) roundedBox([40,15,_thick],5,true);
translate([-10,7.5,3]) cube([20,15,_thick],center=true);
translate([-20,7.5,-21.1+6]) roundedBox([40,15,_thick],5,true);
translate([-10,7.5,-21.1+6]) cube([20,15,_thick],center=true);
}
hull(){
translate([ 20,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
translate([ 22,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
}
hull(){
translate([  -4,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
translate([  -6,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
}
hull(){
translate([-30,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
translate([-32,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
}
color("yellow")  translate([ -30,16,-10])  rotate([90,0,0]) boltHole(8,length=10,tolerance=0.1);
color("yellow")  translate([ -30,7,-10])  rotate([90,0,0]) boltHole(_Mbolt,length=10,tolerance=0.1);
}

}


module profile_mount_motor(){
difference(){
color("green")
union(){
translate([6,0,-27.2]) cube([24.0,_thick,_mod]);
translate([6,-20,0]) cube([24,20,_thick]);
}
hull(){
translate([ 20,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
translate([ 22,-10,-1]) boltHole(_Mbolt,length=10,tolerance=0.1);
}

}

}

