//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/fasteners.scad>
include <std/chain.scad>  // important, include so you can override constants!!
//======================================================================
// Parameters
//======================================================================

$fn = 90;

//======================================================================
//
// Most important values:
//
     inside_dy  =  15; // 12.85, 17,15,12 etc; -  width of inner hole
     inside_dz  =  9; // 9.5, 10, 9, 8; - height of inner hole
     wall_thick = 2.0; // outer wall and foor/roof thickness
//======================================================================
//uncomment for 17x10
//     wall_length = 15.25;
//     overlap     = 4.0;
//     dx2        = 0.35; // 17  10 15.25 4: 0.35
//     br         = 1.5+_tol;// 1.5 for 17x10, 2.25 16.9 mean bump radius
//======================================================================

// attachment plate parameters
     hole = 5; //M size, 4, 5, 6

// fitting
     _prof = 30;// mm Al profile for mountng plates, 20 or 30

// display
     _ShowBolts = 0;

//======================================================================
// main program, comment components as needed
//======================================================================

//chain_plate_small(0,1);
//chain_plate_small_long(0,1);
//chain_plate_small_top(0,1);
//chain_plate_big_long(1,0);
//chain_plate_big_top(1,0);
//chain_link_big_flipz(1,1);
//chain_link_small_flipz(1,1);
//chain_big_slot_3mm(1,0);
//chain_small_slot_3mm(0,1);
chain_link(1,1);
/*
union(){
    translate([-hole_x0,0,bump_z]){
        chain_link_onewall(0, 1);
    translate([hole_x0,0,-bump_z])
         mirror([1,0,0]){
             mirror([1,0,0]) rotate([0,0,180]) wall_hole_flipz(0);
         }
    }
}
    translate([-wall_length+overlap+wall_thick/2,-14,height/2]){
        rotate([90,0,90]){
                roundedBox([16,height,wall_thick],1,true);
        }
      translate([6-wall_thick/2,0,-height/2+wall_thick/2])
          rotate([0,0,90]) {
            difference(){
              cube([16,12,wall_thick],true);
                translate([0,0,-2.5])cylinder(d=5,h=5);
            }
          }
    }
*/
//chain([0,0,0,0,1,1,0,0,1,0,1,0,0]);

//======================================================================

