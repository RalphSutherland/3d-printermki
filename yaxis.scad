//======================================================================
// Uses
//======================================================================
use <std/fasteners.scad>
use <std/skxx.scad>
use <std/scxuu.scad>
use <std/chain.scad>
use <y/ymounts.scad>
use <y/yclamp.scad>
//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

//======================================================================
// main program, comment components as needed
//======================================================================

yaxis(ypos=0,cables=1);

//======================================================================
// modules
//======================================================================

module yaxis(ypos=150, cables = 0){
  h = yrodblockheight(_ydiam)+3;
  dh = h-(yrodblockbelow(_ydiam)+scbelowc(_ydiam));
  dz=h;

//echo(h,dh);
    translate([0,-_platesize/2+ypos+5,0]){
      //  translate([0,0,dz]) ycarriage(width=_platesize,d=_ydiam);
        translate([0,0,dz]) ycarriage(width=_platesize);
        translate([0,0,dz-1]) ysupports(width=_platesize);
        ybearings(d=_ydiam);
        color(_yaxisColor) translate([0,0,dz-yclampheight()]) 
            yclamp();
    }

    yrods(width=_insideWidth, prof=_prof, length=_yrods, sep=_yrodXsep, d=_ydiam);

    translate([0,-w/2, 0]){
        ymotor(length=40.0);
        //belt approx
        color([0.2,0.2,0.2,1.0])translate([0,_insideWidth/2,-8+_h])cube([6,_insideWidth,2],center=true);
        color([0.2,0.2,0.2,1.0])translate([0,_insideWidth/2,-21+_h])cube([6,_insideWidth,2],center=true);

    }

    translate([0,w/2, 0]){
        yidle(d=18,gap=9);
    }

    if (cables == 1){

    translate([-_platesize/2+6,-_platesize/2+ypos+5,dz-8])
    rotate([180,0,90]){
    if (ypos==250){
    chain([0,0,0,0,0,0,0,0,1,1,0,1,1,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==200){
    chain([0,0,0,0,0,0,0,0,1,1,0,1,1,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==150){
    chain([0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,0,
           0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==100){
    chain([0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,
           0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==50){
    chain([0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,
           0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==0){
    chain([0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    if (ypos==-10){
    chain([0,0,0,0,0,0,0,0,0,0,0,
           0,0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,
           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    }
    }
    }// cables

}

// plate
module ycarriage(width=_platesize){
    color("silver")translate([0,0,25+1.5])cube([width,width,3],center=true);
    translate([0,0,28+1.5])cube([width,width,3],center=true);
    color("DimGrey")translate([0,0,1.5])cube([width,width,3],center=true);
}

module ysupports(width=_platesize) {
    sep   = _platesize-40; // mm
    //echo("ysupports sep",sep);
    sh    = sep/2;
    hg    = 34;
    diam  = 5;
    color("yellow"){
    translate([-sh,-sh,0]) cylinder(d=diam,h=hg);
    translate([-sh, sh,0]) cylinder(d=diam,h=hg);
    translate([ sh, sh,0]) cylinder(d=diam,h=hg);
    translate([ sh,-sh,0]) cylinder(d=diam,h=hg);
    }
}

// 3 bearings
module ybearings(sep=_yrodXsep, d=_ydiam) {
    shx    = sep/2;
    shy    = shx-sclength(_ydiam)/2-20;
     translate([0,0,yrodblockbelow(d)+scabovec(d)]){
         translate([-shx,-shy,0]) rotate([0,180,0]) scxxuu(d);
         translate([-shx, shy,0]) rotate([0,180,0]) scxxuu(d);
         translate([ shx, 0,0]) rotate([0,180,0]) scxxuu(d);
         //translate([ shx,-shy,0]) rotate([0,180,0]) scxxuu(d);
    }
}

module yrods(width=380, prof=20, length=420, sep=_yrodXsep, d=_ydiam){
     h    = prof/2;
     w    =  width;
     l    = length;
    sh    =  sep/2;
    tol   =    0.1;

    translate([-sh,(l+tol)/2,yrodblockbelow(d)]) 
       rotate([90,0,0])
           color("Silver")  cylinder(d=d, h=l+tol);
    translate([-sh,-w/2-h,yrodblockbelow(d)])
        color("DimGray") yrodblock(d);
    translate([-sh, w/2+h,yrodblockbelow(d)])
        color("DimGray") yrodblock(d);

    translate([ sh,(l+tol)/2,yrodblockbelow(d)]) 
       rotate([90,0,0])
           color("Silver")  cylinder(d=d, h=l+tol);
    translate([ sh,-w/2-h,yrodblockbelow(d)])
        color("DimGray") yrodblock(d);
    translate([ sh, w/2+h,yrodblockbelow(d)])
        color("DimGray") yrodblock(d);


}
