use <std/boxes.scad>
use <std/fasteners.scad>
use <std/motors.scad>
use <z/ztop.scad>
use <z/zplates.scad>
use <z/zrods.scad>
use <z/zbrace.scad>

include <const.scad>

zaxis();

module zaxis(){

    _zstandoffy = _yoffxz;

    translate([-l/2,-_yoff-_prof, 0])   rotate([ 0,  0,-90]) zbrace(dir=0);
    translate([ l/2,-_yoff-_prof, 0])   rotate([ 0,  0,-90]) zbrace(dir=1);

translate([-l/2,-_yoff, 12]) {
                       translate([ -_moc-1  ,_zstandoffy+_zrodoffsety, 0]) zplateleft(_prof);
    translate([_h, _zstandoffy, -6]) roundedBox([_prof,_zstandoffy*2,12],3,true);
                       translate([ -_moc-1  ,_zstandoffy+_zrodoffsety,-48]) rotate([0,0,90]) stepper_motor(17);
    color("SlateGrey") translate([ -_moc-1  ,_zstandoffy+_zrodoffsety,22]) cylinder(d=9,h=_zleads);
    color("silver")    translate([ -_moc-1  ,_zstandoffy+_zrodoffsety,22-25/2]) cylinder(d=22,h=25);
    color("silver")    translate([ -_moc+_zrodoffsetx-1,_zstandoffy,0]) cylinder(d=_zdiam,h=_zrods);
    translate([ -(_moc+1), (_zstandoffy+6), _insideHeight+7]) ztopplate(1);
}

translate([ l/2, -_yoff, 12]) {
    translate([  _moc+1  ,_zstandoffy+_zrodoffsety,0]) zplateright(_prof);
    translate([-_h, _zstandoffy, -6]) roundedBox([_prof,_zstandoffy*2,12],3,true);
                   translate([  _moc+1  ,_zstandoffy+_zrodoffsety,-48]) rotate([0,0,90]) stepper_motor(17);
    color("SlateGrey") translate([  _moc+1  ,_zstandoffy+_zrodoffsety,22]) cylinder(d=9,h=_zleads);
    color("silver")    translate([ _moc+1  ,_zstandoffy+_zrodoffsety,22-25/2]) cylinder(d=22,h=25);
    color("silver")    translate([  _moc-_zrodoffsetx+1,_zstandoffy, 0]) cylinder(d=_zdiam,h=_zrods);
    translate([ _moc+9, (_zstandoffy+6), _insideHeight+7]) ztopplate(0);
}
}
