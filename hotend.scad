//======================================================================
// Uses
//======================================================================
use <std/fasteners.scad>;
use <std/lmxuu.scad>;
use <std/fans.scad>;

use <x/xcarriage.scad>;
use <x/xblower.scad>;

use <hot/E3D_mount.scad>;
//use <hot/e3d_chimera_v2.scad>;
//use <hot/E3Dv6.scad>;
use <hot/E3Dv5.scad>;

use <hot/irprox-side.scad>;
use <hot/irprox.scad>;

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

bolt_y_centres = _carriageBoltSpacing;//24std,24.25,23
bolt_x_centres = _carriageBoltSpacing;//24std,24.25,23

cwidth  = _carriage_width;
cheight = _carriage_height;
cdepth  = _carriage_axis_depth;

// backing plate
plate_width  = _hotplate_width;//E3Dv5 side prox: 69;
plate_margin = 5.0; // right margin for mounting holes, gives 64mm plate 
plate_height = _hotplate_height;//E3Dv5 side prox: 90;
plate_thick  = _hotplate_thick;//std signboard 3.0;

//
rods    = _xrodZsep; // 44.5/45
_ShowFittings = 1;

//======================================================================
// main program, comment components as needed
//======================================================================

hotend();

//======================================================================
// modules
//======================================================================

module hotend(rods=_xrodZsep){

       translate([ 0, 0, cheight/2]) 
            rotate([ 90, 0, -90]) 
                x_carriage();
  
       plate_xshift =  0;
       plate_yshift = (plate_margin)/2;
       plate_zshift = (plate_height-cheight)/2;//11;
       echo("xyz",plate_xshift,plate_yshift,plate_zshift);

       translate([ plate_thick + plate_xshift, plate_yshift, -plate_zshift]) 
       rotate([  0,  0, 90]) {
            hot_plate(yoff=plate_yshift,zoff=-plate_zshift);
       }

       translate([23, 0, 1.0]) 
            E3Dv5_extruderMounted();
   
    if (_ShowFittings==1){
        translate([0, 0, 0]) {
        // mount retainer
       translate([27,-15.5, 34]) 
           rotate([0,-90,0])
               socketBolt(size=4,length=15);
       translate([27, 15.5, 34]) 
            rotate([0,-90,0])
                socketBolt(size=4,length=15);
       // mount to plate
       translate([7,-bolt_x_centres/2, 22]) 
            rotate([0,-90, 0])
                socketBolt(size=3,length=30);
       translate([7, bolt_x_centres/2, 22]) 
            rotate([0,-90, 0])
                socketBolt(size=3,length=30);
       translate([7,-bolt_x_centres/2, 22+bolt_y_centres]) 
            rotate([0,-90, 0])
                socketBolt(size=3,length=30);
       translate([7, bolt_x_centres/2, 22+bolt_y_centres]) 
            rotate([0,-90, 0])
                socketBolt(size=3,length=30);
            }
    }
    
// side mounted
       translate([3,32,-10])
         rotate([ 0, 0, 90])
           rotate([ 90,0, 0]){
               irprox();
           }

       dz = 0.5*(cheight-rods);
       dy = 0.5*(24.5)+1; //twin_bearing_length and twin_bearing_offset xcarriage

       translate([-cdepth,  dy, dz]) lmxuu(id=8);
       translate([-cdepth, -dy, dz]) lmxuu(id=8);

       translate([-cdepth,  dy, dz+rods]) lmxuu(id=8);
       translate([-cdepth, -dy, dz+rods]) lmxuu(id=8);

// in x-carriage.scad
    translate([-34,0,26.0]) 
    rotate([90, 180,-90])  {
         x_blower_duct(21);
    }
    
//
// m3 mounting holes
// top row
    
      translate([ plate_xshift, 0, plate_height -plate_zshift-4]) {
          translate([-2,                              0,0]) rotate([0,90,0])
              cylinder(d=3.3, h=6);
          translate([-2,-(cwidth)/2,0]) rotate([0,90,0])
              cylinder(d=3.3, h=6);
          translate([-2, (plate_width-plate_margin)/2,0]) rotate([0,90,0])
              cylinder(d=3.3, h=6);
      }
      
      translate([ plate_xshift, 0, -plate_zshift]) {
          translate([-2, (plate_width-plate_margin)/2,8]) rotate([0,90,0])
              cylinder(d=3.3, h=6);
          translate([-2, (plate_width-plate_margin)/2,23]) rotate([0,90,0])
              cylinder(d=3.3, h=6);
      }
 

}


module hot_plate(yoff=0,zoff=0,ChimeraMount=0){
    //echo("hot_plate: ",plate_width,plate_height,3);
    //echo("hot_plate dy=",0.5*( cwidth -  plate_width)-yoff);
    //echo("hot_plate dz=",0.5*(cheight - plate_height)-zoff);
    color("lightgray")
    translate([0,(plate_thick/2)+0.5, plate_height/2]) 
      rotate([90,0,0]){
        difference(){
            cube([plate_width,plate_height,plate_thick],true);
            if (ChimeraMount==1)
                for(i=[[ 4.5,-0.5*plate_height+20,-5],[-4.5,-0.5*plate_height+20,-5],[0,-0.5*plate_height+10,-5]])
                    translate(i) cylinder(d=2,h=10);
            translate([-yoff,0.5*(cheight -plate_height)-zoff,-12]) x_bolt_holes();
         }
    }

}
