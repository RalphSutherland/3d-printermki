//======================================================================
// Uses
//======================================================================
use <std/boxes.scad>
use <std/fasteners.scad>

//======================================================================
// Parameters all dimensions in mm
//======================================================================

$fn = 50;

fan = 40; // mm
bolt = 3 ; // metric size
bolt_len = 24;// mm
tol = 0.1;//mm

thick=5;

wid1 =36.0;
vert1=8.0;
wid2 =20.0;
vert2=36.0;

//======================================================================
// main program, comment components as needed
//======================================================================

   color("red")
   rotate([0,24+90,180])
       objfan();

//======================================================================
// modules
//======================================================================

module fanbolts() {
    space = 0.8*fan; // works for 40 mm, bolts at 32mm
    del=(fan -space)*0.5;
    translate([del,del,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
    translate([del+space,del,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
    translate([del+space,del+space,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
    translate([del,del+space,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
}

module fanwashers() {
    space = 0.8*fan; // works for 40 mm, bolts at 32mm
    del=(fan -space)*0.5;
    translate([del,del,-0.4]) washer(4.0);
    translate([del+space,del,-0.4])  washer(4.0);
    translate([del+space,del+space,-0.4])  washer(4.0);
    translate([del,del+space,-0.4])  washer(4.0);
}

module faninlet(r1,r2,h) {

del=(fan - (r1*2))*0.5;

    translate([r1+del,r1+del,0]){
       cylinder(r1=r1,r2=r2,h=h);
    }

   translate([0,0,-fan]) cube([fan+2,fan+2,fan],center=false);

}



module fanshell (r1,r2,h) {
difference(){
    union(){
    translate([fan*0.5, fan*0.5,thick*0.5+0.01]) roundedBox([fan,fan,thick], 4, true);
    translate([fan*0.5-20, fan*0.5,thick*0.5+0.01]) cube([fan,18,thick],center=true);
    }
    faninlet(r1=wid1*0.5, r2=wid1*0.5, h=vert1);
    fanbolts();
}
}


module objfan() {

difference(){
union(){

    rotate([0,90,180])
    translate([0,-wid1/2-2,-15])
    difference(){

    union(){
        translate([19,0,-1]) rotate([0,24,0]) translate([4,0,0]) fanshell(r1=wid2*0.5,r2=wid2*0.5, h=vert1);
        translate([1.0,0.5*fan,10]) rotate([90,0,0]) roundedBox([17,19,18], 3, true);
        //translate([1.0,0.5*fan-5-thick*0.5,10]) rotate([90,0,0]) roundedBox([18,20,thick], 3, true);
        //translate([1.0,0.5*fan+5+thick*0.5,10]) rotate([90,0,0]) roundedBox([18,20,thick], 3, true);
    }

    translate([1,0.5*fan,-4]) rotate([90,24,0]) translate([20, 0.1, 0]) cube([75,20,50],center=true);

    translate([1, 0.5*(fan+bolt_len), 15]) {

    hull(){
    translate([-3, 0, 0])  rotate([90,0,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
    translate([-5, 0, 0])  rotate([90,0,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
    }

    hull(){
    translate([+5, 0, 0])  rotate([90,0,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
    translate([+3, 0, 0])  rotate([90,0,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
    }

}

//
// translate([0, 0.5*(fan+bolt_len), 5]) {
// translate([-4, 0, 0])  rotate([90,0,0]) boltHole(size=bolt, length=bolt_len, tolerance = tol );
// }
//
   }
}

cube([14,10,22],center=true);

}

}
