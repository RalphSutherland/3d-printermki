use <std/boxes.scad>
use <std/fasteners.scad>

use <chxfans.scad>
use <chxmount.scad>

$fn=60;

bolt_y_centres = 24.;//24.25,23,24
bolt_x_centres = 24.;//24.25,23,24

radiator_diameter = 24.75;// 24.75 chx,  E3Dv5, 22.0 E3Dv6
radiator_top      = 50;// 50 chx,  E3Dv5,42.75 E3Dv6

// Chinese E3D v5 clone - 'all metal jhead'
//CHXtruder();
// chxM6Nozzle();
CHXtruderMounted(fan = 1);
//

module CHXtruderMounted(fan = 1){
   rotate([0,0,180]) CHXtruder();
   translate([0,-25,50])  rotate([0,0,90]) chxSingleMount();
  // translate([0,0,15.5]) rotate([ 90,0,180])coldendfan();
  //  translate([0,0,15.5]) chxFanAssembly(fan=fan);
}

 module chxThermalBarrel(){
 difference(){
 union(){
 cylinder(d=5.9,h=18.75);
 translate([0,0,0])cylinder(d=4,h=22);
 translate([0,0,21])cylinder(d=5.9,h=4.5);
 translate([0,0,25.5])cylinder(d1=5.9,d2=1.75,h=1.0);
 }
  translate([0,0,0])cylinder(d=1.75,h=27);
  translate([0,0,-0.05])cylinder(d1=4,d2=1.75,h=0.5);
 }
}

module chxM6Nozzle(){
 difference(){
     union(){
     intersection(){
      union(){
          cylinder(d1=1.5,d2=7.5,h=4);
          translate([0,0,4])cylinder(d=7.5,h=4);
      }
      cylinder(d=7.5,h=8, $fn=6);
     }
      translate([0,0,8])cylinder(d=6,h=5);
     }
     cylinder(d=0.5,h=20);

 }
}

module chxHeatBlock(){
blockXY=19;
blockZ=10;
difference(){
   cube([blockXY,blockXY,blockZ]);
   translate([blockXY/2,blockXY/2+4,-0.5]) cylinder(d=5,h=blockZ+1);
   translate([blockXY/2,blockXY/2-4.25,blockZ/2+0.5]) cylinder(d=2.1,h=blockZ/2);
   translate([-0.5,5.25,5]) rotate([0,90,0]) cylinder(d=6.5,h=blockXY+1);
   translate([blockXY-blockXY/3+0.5,blockXY/2+4,5.5]) rotate([0,90,0]) cylinder(d=2.5,h=blockXY/3);
}
}

module CHXRadiator(){
    color("lightgray") {

    translate([0,0,46.25])  cylinder(r=8,h=3.75); // top=50
    translate([0,0,37.5])   cylinder(r=6,h=12.5); // top=50
    translate([0,0,37.5])   cylinder(r=8,h=3);    // bottom=50-12.5
    translate([0,0,34])     cylinder(r=8,h=1.0);  // bottom=50-16

    cylinder(d=14.5,h=18);
    cylinder(d=12.5,h=21);
    cylinder(d=   9,h=50);

    for (i = [0:9]) {
        translate([0,0,(31/9)*i]) cylinder(d=radiator_diameter,h=1.0);
    }

   }

}

module CHXtruder(){

    color("white") translate([0,0,60])     cylinder(r=1.5,h=10); // inlet
    color("Blue") translate([0,0,59])     cylinder(r=5,h=1); // inlet
    color("DarkGoldenrod") translate([0,0,50])     cylinder(r=4,h=9); // inlet
    color("DarkGoldenrod") translate([0,0,50])     cylinder(r=5,h=8,$fn=6); // inlet

    CHXRadiator();

    color("slategray") translate([0,0,17 ]) rotate([180,0,0]) chxThermalBarrel();
 
    color("lightgray")     translate([-9.5,-13.5,-15])  chxHeatBlock();
    color("DarkGoldenrod") translate([0,0,-23])         chxM6Nozzle();

}



