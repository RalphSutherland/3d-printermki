use <std/boxes.scad>
use <std/fasteners.scad>

    $fn=60;
    bolt_y_centres = 24.;//24.25,23,24
    bolt_x_centres = 24.;//24.25,23,24

    translate([0,-14,0]) chxSingleMount();
    //translate([0, 14,0]) mirror([0,1,0])hotmount();

module chxSingleMount(){
rotate([180,0,0])
    difference(){

union(){
  plate();
  bracket();
  translate([10.0,0,20]) {
  difference(){
       cube([10,25,10],true);
       translate([6,13,7]) rotate([90,0,0]) cylinder(r=10,h=26);
  }
  }

}

translate([0,0,5]){
    translate([-1,bolt_x_centres/2,bolt_y_centres]) {
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=7,$fn=6);
    }
    translate([-1,bolt_x_centres/2,0]){
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=7,$fn=6);
    }
    translate([-1,-bolt_x_centres/2,bolt_y_centres]) {
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=7,$fn=6);
    }
    translate([-1,-bolt_x_centres/2,0]) {
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=7,$fn=6);
    }
}

}

}

module plate(){

    h=35.5;
    w=bolt_x_centres+12;
    t=8.0;
    translate([t/2,0,h/2])
    rotate([0,90,0]){
        union(){
        roundedBox([h,w,t],3,true);
         translate([h/4,0,0])cube([h/2,w,t],true);
        }
    }
}

module bracket(){

    d=16.0;
    h=17.25;
    l=25.0;
    l2=16.0;
    difference(){
    union(){
        translate([l/2,0,h/2])cube([l,l,h],true);
        translate([l,0,0])cylinder(d=l,h=h);
        translate([l+l2/2+7.25,0,h/2])rotate([90,0,0])roundedBox([14.5,h,10],3,true);
    }//union
    translate([l,0,-2.5])cylinder(d=l2+0.5,h=h+2);
    translate([l,0,-1.5])cylinder(d=l2-0.5,h=h+2);
    translate([l+16,0,(h/2)])cube([20,0.75,h+2],true);
    translate([41,10,h/2+4])rotate([90,0,0])boltHole(3,length=20,tolerance=0.1);
    translate([41,10,h/2-4])rotate([90,0,0])boltHole(3,length=20,tolerance=0.1);
    }//diff
}

module brace(){
  linear_extrude(height=5){
      polygon([[0,0],[10,0],[0,10]]);
  }
}
