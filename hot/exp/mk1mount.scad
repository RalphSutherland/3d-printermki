use <std/boxes.scad>
use <std/fasteners.scad>

//use <objfan.scad>
//use <chxfans.scad>

    $fn=50;
    
//mk1FanAssembly(fan=0);
//translate([0,-16,28]) rotate([0,0,-90])
//    objfan();
  mk1hotmount();
  // translate([0, 14,0]) mirror([0,1,0])hotmount();

module mk1hotmount(){
difference(){
    union(){

    mk1plate();
    mk1bracket();

    translate([8.5,0,19.5]) {

    difference(){
            cube([5,25,5],true);
            translate([2.5,13,2.5]) rotate([90,0,0]) cylinder(r=5,h=26);
        }
    }

    translate([6,0,17])translate([-0.5,2.5,-0.5])rotate([90,0,0])mk1brace();

    }

    translate([0,-14,0]){

    translate([-1,5,20]) rotate([0,90,0]) nutHole(size=4);//cylinder(d=8,h=4,$fn=6);
    translate([-1,5,20]) rotate([0,90,0]) cylinder(d=4,h=10);

hull(){
    translate([-1,7.5,28]) rotate([0,90,0]) cylinder(d=4,h=10);
    translate([-1,7.5,26]) rotate([0,90,0]) cylinder(d=4,h=10);
}
hull(){
    translate([-1,-7.5,28]) rotate([0,90,0]) cylinder(d=4,h=10);
    translate([-1,-7.5,26]) rotate([0,90,0]) cylinder(d=4,h=10);
}

    }

    translate([0,14,0]) mirror([0,1,0]){

    translate([-1,5,20]) rotate([0,90,0]) nutHole(size=4);//cylinder(d=8,h=4,$fn=6);
    translate([-1,5,20]) rotate([0,90,0]) cylinder(d=4,h=10);

hull(){
    translate([-1,7.5,28]) rotate([0,90,0]) cylinder(d=4,h=10);
    translate([-1,7.5,26]) rotate([0,90,0]) cylinder(d=4,h=10);
}
hull(){
    translate([-1,-7.5,28]) rotate([0,90,0]) cylinder(d=4,h=10);
    translate([-1,-7.5,26]) rotate([0,90,0]) cylinder(d=4,h=10);
}

    }

}

}

module mk1plate(){
    
    h=35.5;// 35.5, 70
    w=50.0;
    t=6;
    translate([t/2,0,h/2]) // h/2 -2
    rotate([0,90,0]){
        union(){
        roundedBox([h,w,t],3,true);
         //translate([h/4,0,0])cube([h/2,w,t],true);
        }
    }
}

module mk1bracket(){
    
    d=16.0;
    h=17.25;
    l=25.0;
    l2=16.0;
    difference(){
    union(){
        translate([l/2,0,h/2])cube([l,l,h],true);
        translate([l,0,0])cylinder(d=l,h=h);
        translate([l+l2/2+7.25,0,h/2])rotate([90,0,0])roundedBox([14.5,h,10],3,true);
    }//union
    translate([l,0, 1]) cylinder(d=l2+0.1,h=h+2);
    translate([l,0,-1]) cylinder(d=l2-0.5,h=h+2);
    translate([l+16,0,(h/2)])cube([20,0.75,h+2],true);
    translate([41,10,h/2+4]) rotate([90,0,0])boltHole(3,length=20,tolerance=0.1);
    translate([41,10,h/2-4]) rotate([90,0,0])boltHole(3,length=20,tolerance=0.1);
    }//diff
}

module mk1brace(){
  linear_extrude(height=5){
      polygon([[0,0],[10,0],[0,10]]);
  }
}