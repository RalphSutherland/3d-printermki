use <std/boxes.scad>
use <std/fasteners.scad>
use <chxmount.scad>
use <mk1mount.scad>

$fn=60;

bolt_y_centres = 24.;//24.25,23,24
bolt_x_centres = 24.;//24.25,23,24

radiator_diameter = 24.75 ;//// 24.75 chx,  E3Dv5, 22.0 E3Dv6

_showFans = 1;
//

//chxFanAssembly(fan=1);
coldendfan0();
//mk1FanBack(fan=0);
//mk1FanFront(fan=0);
//chxFanFront();

module chxFanFront(fan=0){

    difference(){
    intersection(){
     translate([-25,-20,0])cube([50,40,20]);
    union(){
        rotate([ 0,0,  0]) coldendfan0(fan);
        rotate([ 0,0,180]) coldendfan0(fan);
        }
    }
    h=17.25;
    l=26;

     translate([0,25,0])rotate([90,-90,0]){
      translate([-l/2,0,0])cube([l,l,h],true);
      translate([0,0,-h/2])cylinder(d=l,h=h);
//        cylinder(d=26,h=10);
     }

translate([ 5.4,-9.0,-2.4]) hull(){
    boltHole(3, length= 30);//cylinder(r=2.0,h=20);
   // translate([0,6.5,0.0]) boltHole(3, length= 30);//cylinder(r=2.1,h=20);
}

translate([-5.4,-9.0,-2.4]) hull(){
    boltHole(3, length= 30);//cylinder(r=2.0,h=20);
    //translate([0,6.5,0.0]) boltHole(3, length= 30);
    //cylinder(r=2.1,h=20);
}


translate([ 5.4,-9.0,11]) hull(){
    //boltHole(5, length= 10);
    nutHoleLocknut(3,tolerance =0.1);
    //cylinder(r=2.0,h=20);
   // translate([0,6.5,0.0]) boltHole(5, length= 10);//cylinder(r=2.1,h=20);
}
translate([-5.4,-9.0,11]) hull(){
    //boltHole(5, length= 10);//cylinder(r=2.0,h=20);
    nutHoleLocknut(3,tolerance =0.1);
    //translate([0,6.5,0.0]) boltHole(5, length= 10);
    //cylinder(r=2.1,h=20);
}


}
}



module chxFanBack(fan=0){
    difference(){
    union(){
    intersection(){
        translate([-25,-20,-20])cube([50,40,20]);
        union(){
        rotate([ 0,0,  0]) coldendfan(fan);
        rotate([ 0,0,180]) coldendfan(fan);
        }
    }

     translate([0,34.5,-25])rotate([90,0,180]) rotate([0,0,90]) {
         chxSingleMount();
     }

     }// union

     translate([0,34.5,-25])rotate([90,0,180]) rotate([0,0,90]) {
         rotate([180,0,0])
    translate([0,0,5]){
    translate([-1,bolt_x_centres/2,bolt_y_centres]) {
         rotate([0,90,0]) cylinder(d=3.6,h=20);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=24,$fn=6);
    }
    translate([-1,bolt_x_centres/2,0]){
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=4,$fn=6);
    }
    translate([-1,-bolt_x_centres/2,bolt_y_centres]) {
         rotate([0,90,0]) cylinder(d=3.6,h=20);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=24,$fn=6);
    }
    translate([-1,-bolt_x_centres/2,0]) {
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=4,$fn=6);
    }
    }
    }

}// diff
}


module mk1FanFront(fan=0){

    difference(){
    intersection(){
     translate([-25,-20,0])cube([50,40,20]);
    union(){
        rotate([ 0,0,  0]) coldendfan(fan);
        rotate([ 0,0,180]) coldendfan(fan);
        }
    }
    h=17.25;
    l=26;

     translate([0,25,0])rotate([90,-90,0]){
      translate([-l/2,0,0])cube([l,l,h],true);
      translate([0,0,-h/2])cylinder(d=l,h=h);
//        cylinder(d=26,h=10);
     }

    }
}




module mk1FanBack(fan=0){
    difference(){
    union(){
    intersection(){
        translate([-25,-20,-20])cube([50,40,20]);
        union(){
        rotate([ 0,0,  0]) coldendfan(fan);
        rotate([ 0,0,180]) coldendfan(fan);
        }
    }

     translate([0,18,-25])rotate([90,0,180]) rotate([0,0,90]) {
         mk2hotmount();
     }

     }// union
 /*
     translate([0,34.5,-25])rotate([90,0,180]) rotate([0,0,90]) {
         rotate([180,0,0])
    translate([0,0,5]){
    translate([-1,bolt_x_centres/2,bolt_y_centres]) {
         rotate([0,90,0]) cylinder(d=3.6,h=20);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=24,$fn=6);
    }
    translate([-1,bolt_x_centres/2,0]){
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=4,$fn=6);
    }
    translate([-1,-bolt_x_centres/2,bolt_y_centres]) {
         rotate([0,90,0]) cylinder(d=3.6,h=20);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=24,$fn=6);
    }
    translate([-1,-bolt_x_centres/2,0]) {
         rotate([0,90,0]) cylinder(d=3.6,h=10);
        translate([6,0,0]) rotate([0,90,0]) cylinder(d=6.8,h=4,$fn=6);
    }
    }
    }
*/

}// diff
}




module chxFanAssembly(fan=_showFans){
    rotate([90,0,0]){
    color("red") chxFanFront(fan);
    translate([0,0,-0.25]) color("blue") chxFanBack(fan);


   if (fan==1) {

       rotate([0,90,0])
       translate([0,0,-25])  {
       translate([-20,20,-10]) rotate([90,0,0]) fan4010();


    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -10])  rotate([0,0,0]) socketBolt(size=3,length=15);
    }
    }

    }

       rotate([0,-90,0])
       translate([0,0,-25])  {
       translate([-20,20,-10]) rotate([90,0,0]) fan4010();


    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -10])  rotate([0,0,0]) socketBolt(size=3,length=15);
    }
    }

    }

   }// fan == 1

    }

}






module mk1FanAssembly(fan=_showFans){
    rotate([90,0,0]){
    color("red") mk1FanFront(fan);
    translate([0,0,-0.25]) color("green") mk1FanBack(fan);


   if (fan==1) {

       rotate([0,90,0])
       translate([0,0,-25])  {
       translate([-20,20,-10]) rotate([90,0,0]) fan4010();


    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -10])  rotate([0,0,0]) socketBolt(size=3,length=15);
    }
    }

    }

       rotate([0,-90,0])
       translate([0,0,-25])  {
       translate([-20,20,-10]) rotate([90,0,0]) fan4010();


    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -10])  rotate([0,0,0]) socketBolt(size=3,length=15);
    }
    }

    }

   }// fan == 1

    }

}





module coldendfan0(fan=0){
//rotate([0,90,0])
//translate([0,0,-25])  
rotate([0,0,0])
translate([0,0,-20])  
    {
   {
//
//intersection(){
//    translate([0,25,3]) rotate([90,0,0]) scale([0.58,1,1]) cylinder(d=70,h=50);

  union(){
   difference(){
    union(){
    hull(){
        translate([0,0, 3]) roundedBox([40,40,6],2,true);
       translate([0,0, 5]) rotate([90,0,0]) cube([36,6,38],center=true);
    }
    hull(){
       translate([0,0, 9]) rotate([90,0,0]) cube([36,6,37],center=true);
       translate([0,0,22]) rotate([90,0,0]) cube([30,6,32],center=true);
    }
    } // union
    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -0.5]) cylinder(d=3.3,h=10,$fn=6);
        translate([i*17, j*17, 6]) cylinder(d1=9,d2=4,h=25);
    }
    }

    hull(){
    translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=25);
    translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=25);
    translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=25);
    }
    translate([0, 25,20]) rotate([90,0,0]) cylinder(d=radiator_diameter+0.25,h=50);
}
 translate([-12.5, 0,25]) rotate([0,40,0]) rotate([90,0,0]) roundedBox([3,5,1],1, true);
 translate([ 12.5, 0,25]) rotate([0,40,0]) rotate([90,0,0]) roundedBox([5,3,1],1, true);//cube([5,1,5]);
}
//} //intersection
//
   }
  }// trans
}


module coldendfan(fan=0){
rotate([0,90,0])
translate([0,0,-25])  {
   {
//
//intersection(){
//    translate([0,25,3]) rotate([90,0,0]) scale([0.58,1,1]) cylinder(d=70,h=50);

  union(){
   difference(){
    union(){
    hull(){
        translate([0,0, 3]) roundedBox([40,40,6],2,true);
        translate([0,0, 5]) rotate([90,0,0]) cube([36,6,38],center=true);
    }
    hull(){
        translate([0,0, 9]) rotate([90,0,0]) cube([40,6,37],center=true);
        translate([0,0,22]) rotate([90,0,0]) cube([40,6,32],center=true);
    }
    } // union
    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -0.5]) cylinder(d=3.0,h=15,$fn=6);
        //translate([i*16, j*17, 6]) cylinder(d=7,h=10);
    }
    }

    hull(){
    translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=25);
    translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=25);
    translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=25);
    }
    translate([0, 25,25]) rotate([90,0,0]) cylinder(d=radiator_diameter+0.25,h=50);
}
 translate([-14, 0,25]) rotate([0,45,0]) rotate([90,0,0]) roundedBox([5,5,1],1, true);
 translate([ 14, 0,25]) rotate([0,45,0]) rotate([90,0,0]) roundedBox([5,5,1],1, true);//cube([5,1,5]);
}
//} //intersection
//
   }
  }// trans
}

module fan4010(){

  color("DimGray")
  difference(){
    rotate([90,0,0])translate([20,20,-5])roundedBox([40,40,10],2,true);
    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([20+i*16, 10.5, 20+j*16]) rotate([90,0,0]) cylinder(d=4.4,h=11);
    }
    }

    translate([20, 10.5, 20]) rotate([90,0,0]) cylinder(d=38,h=11);
}

  color("Gray")
 intersection (){

        translate([20, 10.5, 20]) rotate([90,0,0]) cylinder(d=37.,h=11);

    translate([20, 10, 20]) {

        rotate([90,0,0]) cylinder(d=17,h=10);

     translate([0, -5, 0])
        for (a=[0,1,2,3,4,5]){
        rotate([0,60*a,0]) rotate([0,0,-30]) translate([-5,-1,0]) cube([10,2,19]);

     }
 }

 }


}

