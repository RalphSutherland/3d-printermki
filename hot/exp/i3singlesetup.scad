use <std/boxes.scad>
use <std/fasteners.scad>

use <chx.scad>
use <chxfans.scad>
use <objfan.scad>
$fn = 50;

//translate([0,0,33]) 
//rotate([0,0,90]) 
//translate([-25,0,0])
//mk1hotmount();
// Chinese E3D v5 clone
CHXtruder();
translate([0,0,15]) {
mk1FanAssembly(fan=1);
translate([0,-16,28]) rotate([0,0,-90])
    objfan();
}

