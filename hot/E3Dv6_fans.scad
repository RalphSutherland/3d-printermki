use <std/boxes.scad>
use <std/fasteners.scad>
use <std/fans.scad>
use <E3D_mount.scad>
use <E3Dv6.scad>

$fn=60;

bolt_y_centres = 24.;//24.25,23,24
bolt_x_centres = 24.;//24.25,23,24

radiator_diameter = 22;//

_showFans = 0;
//
 //E3Dv6_FanFront(fan=0);
 //rotate([180,0,0])E3Dv6_FanBack(fan=0);
 E3Dv6_FanAssembly(fan=1);
translate([ 0, 0, -18]) rotate([0,0,-90]) E3Dv6_extruder();

module E3Dv6_FanFront(fan=0){

    difference(){
    union(){

         intersection(){
             translate([-25,-20,0.1])cube([50,40,20]); // tol 0.1
             union(){
                 rotate([ 0,0,  0]) coldendfan0(fan);
                 rotate([ 0,0,180]) coldendfan0(fan);
             }
         }

     difference(){
         translate([0,34.5,-20])rotate([90,0,180]) rotate([0,0,90]) {
             E3D_MountRetainer();
         }
         rotate([0,90,0])
         translate([0,0,-25])
             hull(){
                 translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=26);
                 translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
                 translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
             }
         rotate([0,-90,0])
         translate([0,0,-25])
             hull(){
                 translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=26);
                 translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
                 translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
             }
     }//diff



     }

translate([ 5.4,-7.0,-2.4]) {
    boltHole(3, length= 30);
}

translate([-5.4,-7.0,-2.4]) hull(){
    boltHole(3, length= 30);
}


translate([ 5.4,-7.0,11]) hull(){
    nutHoleLocknut(3,tolerance =0.1);
    translate([0,0,-3]) nutHoleLocknut(3,tolerance =0.1);
}
translate([-5.4,-7.0,11]) hull(){
    nutHoleLocknut(3,tolerance =0.1);
    translate([0,0,-3]) nutHoleLocknut(3,tolerance =0.1);
}


}
}

module E3Dv6_FanBack(fan=0){
    difference(){
    union(){
    intersection(){
        translate([-25,-20,-20])cube([50,40,20]);
        union(){
        rotate([ 0,0,  0]) coldendfan(fan);
        rotate([ 0,0,180]) coldendfan(fan);
        }
    }

     difference(){
         translate([0,34.5,-20])rotate([90,0,180]) rotate([0,0,90]) {
             E3D_MountBack(high=1);
         }
         rotate([0,90,0])
         translate([0,0,-25])
             hull(){
                 translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=26);
                 translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
                 translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
             }
         rotate([0,-90,0])
         translate([0,0,-25])
             hull(){
                 translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=26);
                 translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
                 translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
             }
     }//diff

     }// union

     translate([0,34.5,-20])rotate([90,0,180]) rotate([0,0,90]) {
         rotate([180,0,0])
         # translate([0,0,5]){
              translate([-1,bolt_x_centres/2,bolt_y_centres]) {
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
              translate([-1,bolt_x_centres/2,0]){
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
              translate([-1,-bolt_x_centres/2,bolt_y_centres]) {
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
              translate([-1,-bolt_x_centres/2,0]) {
                   rotate([0,90,0]) boltHole(size=3,length=10,tolerance=0.1);//cylinder(d=3.6,h=10);
                  translate([6,0,0]) rotate([0,90,0]) washerHole(size=3,length=3,tolerance=0.2);//cylinder(d=6.8,h=7,$fn=6);
              }
          }
    }

// wiring duct
    translate([00,10,-18.5 ])rotate([90,0,0]) cube([6.5,3.1,70],true);

}// diff
}

module E3Dv6_FanAssembly(fan=_showFans){

    rotate([90,0,0]){
    color("red") E3Dv6_FanFront(fan);
    color("blue") E3Dv6_FanBack(fan);

   if (fan==1) {

       rotate([0,90,0])
       translate([0,0,-25])  {
       translate([-20,20,-10]) rotate([90,0,0]) fan4010();


    for (i=[-1,1]){
        for (j=[-1,1]){
            translate([i*16, j*16, -10])  rotate([0,0,0]) socketBolt(size=3,length=15);
        }
    }

    }

        rotate([0,-90,0])
        translate([  0, 0,-25])  {
        translate([-20,20,-10]) rotate([90,0,0]) fan4010();

        for (i=[-1,1]){
            for (j=[-1,1]){
                translate([i*16, j*16, -10])  rotate([0,0,0]) socketBolt(size=3,length=15);
            }
        }

    }

   }// fan == 1

    }

}

module coldendfan0(fan=0){
rotate([0,90,0])
translate([0,0,-25])  {
   {

  union(){
   difference(){
    union(){
    hull(){
        translate([0,0, 2.9])  roundedBox([40,40,6],2,true);
        translate([0,0, 5])  roundedBox([40,37,6],2,true);//rotate([90,0,0]) cube([36,6,38],center=true);
    }
    hull(){
       translate([0,0, 7]) roundedBox([40,37,1],2,true);///rotate([90,0,0]) cube([36,6,37],center=true);
       translate([0,0,22])  roundedBox([40,37,6],2,true);//rotate([90,0,0]) cube([36,6,37],center=true);
    }
    } // union
    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -0.5]) cylinder(d=3.2,h=10,$fn=6);
       // translate([i*17, j*17, 6]) cylinder(d1=9,d2=4,h=25);
    }
    }

    hull(){
        translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=26);
        translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
        translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
    }
    translate([0, 25,25]) rotate([90,0,0]) cylinder(d=radiator_diameter+0.25,h=50);
}
 translate([-14, 1,25]) rotate([0,45,0]) rotate([90,0,0]) roundedBox([5,5,1],1, true);
 translate([ 14, 1,25]) rotate([0,45,0]) rotate([90,0,0]) roundedBox([5,5,1],1, true);//cube([5,1,5]);
}
//} //intersection
//
   }
  }// trans
}


module coldendfan(fan=0){
rotate([0,90,0])
translate([0,0,-25])  {
   {
//
//intersection(){
//    translate([0,25,3]) rotate([90,0,0]) scale([0.58,1,1]) cylinder(d=70,h=50);

  union(){
   difference(){
    union(){
    hull(){
        translate([0,0, 2.9])  roundedBox([40,40,6],2,true);
        translate([0,0, 5])  roundedBox([40,37,6],2,true);//rotate([90,0,0]) cube([36,6,38],center=true);
    }
    hull(){
       translate([0,0, 7]) roundedBox([40,37,1],2,true);///rotate([90,0,0]) cube([36,6,37],center=true);
       translate([0,0,22])  roundedBox([40,37,6],2,true);//rotate([90,0,0]) cube([36,6,37],center=true);
    }
    } // union

    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*16, j*16, -0.5]) cylinder(d=3.2,h=15,$fn=6);
        //translate([i*16, j*17, 6]) cylinder(d=7,h=10);
    }
    }

    hull(){
        translate([0, 0, -0.5]) cylinder(d1=38,d2=radiator_diameter+0.25,h=26);
        translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
        translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=26);
    }
    translate([0, 25,25]) rotate([90,0,0]) cylinder(d=radiator_diameter+0.25,h=50);
   }
 translate([-14, 1,25]) rotate([0,45,0]) rotate([90,0,0]) roundedBox([5,5,1],1, true);
 translate([ 14, 1,25]) rotate([0,45,0]) rotate([90,0,0]) roundedBox([5,5,1],1, true);//cube([5,1,5]);
}
//} //intersection
//
   }
  }// trans
}
