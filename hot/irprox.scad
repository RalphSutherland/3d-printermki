//======================================================================
// Uses
//======================================================================
use  <std/fasteners.scad>;
//======================================================================
// Parameters all dimensions in mm
//======================================================================

$fn = 60;

//====================================================================================
//
// options and dims
//

_maxmicron =  2; // 1 = mxmc x mount, 0 = E3D front mount, 2 = side flat
_lid       =  1; // 1 for a cover, 0 for open - ie low profile
_gap    =  7.9; //interior vertical space ,  7.9 with lid < 4.9 for no bridges and no lid, eg 4.0


// dims, all in mm

_tol = 0.15;

_top     = (_lid>0)?1.0:0.0;// lid thickness if used
_wall    =  _gap+_top; // interior wall vertical space

_base   = 2.0; // > 1.5
_board = _base +_tol; // 1.6 + 0.1 tolerance
_bheight=_wall   - _board; // to approx flush top
_sheight=_base + _wall; // overall shell height

_l1=20.9;
_l2=23.1; // inside width
_l3=25.6; // outside width

//======================================================================
// main program, comment components as needed
//======================================================================
//
// main mounting bracket:
irprox();
//irshell();

//  retaining bar:
// uncomment next line for in place modelling

//irbar();

// uncomment next line for offset and rotated bar for printing
//
//   translate( [35,-20,_board+_base+_bheight]) rotate([180,0,0]) irbar();
//
//======================================================================
// modules
//======================================================================
//
module irprox(){
    // wrapper for calling from includers
    irshell();
    irbar();
// uncomment next line for offset and rotated bar for printing
}

module irbar(){

// clamp bar to hold sensor in bracket
// has a small undercut to prevent damage
// to board IC
// orient upper side down for printing!


difference(){

// tapered hull of 4 cylinder for overall shape

color("blue")    union(){
 hull(){
        translate([ 12.0,  0.000,_board+_base])cylinder(r=3.3,h=_bheight);
        translate([-12.0,  0.000,_board+_base])cylinder(r=3.3,h=_bheight);
        translate([ 10.0, -1.175,_board+_base])cylinder(r=3.3,h=_bheight);
        translate([-10.0, -1.175,_board+_base])cylinder(r=3.3,h=_bheight);
    }
    if (_lid>0){
    translate([-0.5*_l2+_tol,-_l1,_bheight+_base+_board-1.0])cube([_l2-(2*_tol),_l1,1.0]);
    }
}

if (_maxmicron == 0){
    translate([0,14.5,0])cylinder (d = 30,h = _sheight+0.5);
}


if (_maxmicron == 2){
    translate([0,14.5,0])cylinder (d = 30,h = _sheight+0.5);
}

// IC guard gap, 0.5mm
hull(){
        translate([ 6.0,-4.5,_board+_base-0.1])cylinder(r=1.5,h=0.6);
        translate([-6.0,-4.5,_board+_base-0.1])cylinder(r=1.5,h=0.6);
}

// M3 mounting bolt holes
translate([ 12.0,0.0,-2.4]){
        boltHole(3.2, length= 20);
}
translate([-12.0,0.0,-2.4]){
        boltHole(3.2, length= 20);
}

if (_maxmicron == 1){
// Two Adjustable mounting point slots
translate([ 5.4,3.0,-2.4]) hull(){
        boltHole(3,length=20);//cylinder(r=2.1,h=20);
        translate([0,6.5,0.0]) boltHole(3,length=20);
}
translate([-5.4,3.0,-2.4]) hull(){
    boltHole(3,length=20);
    translate([0,6.5,0.0]) boltHole(3,length=20);
    }
}

if (_maxmicron == 2){
// Two Inline Adjustable mounting point slots
translate([ 0,3.0,-2.4]) 
    hull(){
        boltHole(3,length=20);//cylinder(r=2.1,h=20);
        translate([0,6.5,0.0]) boltHole(3,length=20);
      }
translate([ 0,3.0,-2.4]) 
    hull(){
        boltHole(3,length=20);//cylinder(r=2.1,h=20);
        translate([0,6.5,0.0]) boltHole(3,length=20);
      }
}

} // end diff

}

// the main bracket/shell
module irshell(){

// Aus3D IR sensor mounting bracket for Max Micron like X-carriages

// make the shape and subtract the bits we don't want
difference(){

union(){
    hull(){
        translate([ 12.0, 0.000,0.0])cylinder(r=4.6,h=_sheight);
        translate([-12.0, 0.000,0.0])cylinder(r=4.6,h=_sheight);
        translate([ 10.0,-1.175,0.0])cylinder(r=4.6,h=_sheight);
        translate([-10.0,-1.175,0.0])cylinder(r=4.6,h=_sheight);
    }
    translate([-0.5*_l3,-_l1,0.0])cube([_l3,_l1,_sheight]);
    if (_maxmicron==1){
     hull(){
        translate([ 6.0,  0.0,0.0])cylinder(r=3,h=_sheight);
        translate([-6.0,  0.0,0.0])cylinder(r=3,h=_sheight);
        translate([ 6.0,10.0,0.0])cylinder(r=3,h=_sheight);
        translate([-6.0,10.0,0.0])cylinder(r=3,h=_sheight);
     }
    }
    

    if (_maxmicron==2){
     hull(){
        translate([ 6.0,  0.0,0.0])cylinder(r=3,h=_base*2);
        translate([-6.0,  0.0,0.0])cylinder(r=3,h=_base*2);
        translate([ 4.0,28.0,0.0])cylinder(r=3,h=_base*2);
        translate([-4.0,28.0,0.0])cylinder(r=3,h=_base*2);
     }
    }

}

if (_maxmicron == 0){
    translate([0,15,-0.5])cylinder (d = 28,h = _sheight+1);
}

if (_maxmicron == 2){
    translate([0,15,_base*2])cylinder (d = 28,h = _sheight+1);
    translate([0,20,_base*2])cylinder (d = 28,h = _sheight+1);
}

// include cutaway for fan inlet
if (_maxmicron == 0){
    difference(){
    union(){
    hull(){
        translate([ 12.0,  0.000,_base ])cylinder(r=3.6,h=_sheight+0.01);
        translate([-12.0,  0.000,_base ])cylinder(r=3.6,h=_sheight+0.01);
        translate([ 10.0, -1.175,_base ])cylinder(r=3.6,h=_sheight+0.01);
        translate([-10.0, -1.175,_base ])cylinder(r=3.6,h=_sheight+0.01);
    }
    translate([ 0.0,-12.0,(0.5*_sheight)+_base]) cube([_l2+_tol,20.00,_sheight+0.01],center=true);
    }
    translate([0,15,0])cylinder (d = 30,h = _sheight+0.5);
    }
}

if (_maxmicron == 1 ){
    union(){
    hull(){
        translate([ 12.0,  0.000,_base ])
            cylinder(r=3.5,h=_sheight+0.01);
        translate([-12.0,  0.000,_base ])
            cylinder(r=3.5,h=_sheight+0.01);
        translate([ 10.0, -1.175,_base ])
            cylinder(r=3.5,h=_sheight+0.01);
        translate([-10.0, -1.175,_base ])
            cylinder(r=3.5,h=_sheight+0.01);
    }
    translate([ 0.0,-12.0,(0.5*_sheight)+_base]) cube([_l2+_tol,20.00,_sheight+0.01],center=true);
    }
}

if (_maxmicron == 2){
    difference(){
    union(){
    hull(){
        translate([ 12.0,  0.000,_base ])cylinder(r=3.6,h=_sheight+0.01);
        translate([-12.0,  0.000,_base ])cylinder(r=3.6,h=_sheight+0.01);
        translate([ 10.0, -1.175,_base ])cylinder(r=3.6,h=_sheight+0.01);
        translate([-10.0, -1.175,_base ])cylinder(r=3.6,h=_sheight+0.01);
    }
    translate([ 0.0,-12.0,(0.5*_sheight)+_base]) cube([_l2+_tol,20.00,_sheight+0.01],center=true);
    }
    translate([0,15,0])cylinder (d = 30,h = _sheight+0.5);
    }
}


// trench to allow for short component legs near lower edge
translate([ -0.5*(_l2+_tol),-20.5,_base-1.0])cube([_l2+_tol,5.00,1.01]);
color("red") translate([ 8.0,-14.0,_base + _board ])cube([5.5,8.95,3.2]);

// M3 mounting holes
translate([ 12.0,0.0,-2.4]){
    boltHole(3, length= 30);
}
translate([-12.0,0.0,-2.4]){
    boltHole(3, length= 30);
}

if ( _maxmicron == 1){
// Two Adjustable mounting point slots
translate([ 5.4,3.0,-2.4]) hull(){
    boltHole(3, length= 30);//cylinder(r=2.0,h=20);
    translate([0,6.5,0.0]) boltHole(3, length= 30);//cylinder(r=2.1,h=20);
}
translate([-5.4,3.0,-2.4]) hull(){
    boltHole(3, length= 30);//cylinder(r=2.0,h=20);
    translate([0,6.5,0.0]) boltHole(3, length= 30);
    //cylinder(r=2.1,h=20);
}
}

if ( _maxmicron == 2){
// Two Adjustable mounting point slots
translate([ 0,20.0,-2.4]) hull(){
    boltHole(3, length= 30);//cylinder(r=2.0,h=20);
    translate([0,6.0,0.0]) boltHole(3, length= 30);//cylinder(r=2.1,h=20);
}

translate([ 0,5.0,-2.4]) hull(){
    boltHole(3, length= 30);//cylinder(r=2.0,h=20);
    translate([0,6.0,0.0]) boltHole(3, length= 30);//cylinder(r=2.1,h=20);
}

}

} // end diff

} // end shell
