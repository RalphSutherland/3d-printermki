//======================================================================
// Uses
//======================================================================

use <std/boxes.scad>
use <std/fasteners.scad>
use <std/fans.scad>
use <E3D_mount.scad>

//======================================================================
// Parameters all dimensions in mm
//======================================================================

    $fn=60;

    _tol=0.15;
    _ShowFittings = 1;
    _ShowFan = 1;

    radiator_diameter      = 24.75;
    radiator_top           = 50.25;
    radiator_fittingHeight = 12.75;

    extruder_height=radiator_top-0.25;//50 to top of bracket

//======================================================================
// main program, comment components as needed
//======================================================================

 //   E3Dv5_extruder();
    E3Dv5_extruderMounted();
 //   E3Dv5_fanMount();

//======================================================================
// modules
//======================================================================

module E3Dv5_extruderMounted(fan=0){
    
     translate([ 0, 0, -10]) 
     rotate([0,0,90]) 
         E3Dv5_extruder();
    
     translate([-20, 0, extruder_height])
         E3D_Mount();

     translate([0, 0, 6])
     rotate([90,0,-90]){
         E3Dv5_fanMount(fan=_ShowFan);
     }
     
}

module E3Dv5_extruder(){
     //translate([0, 0,  -25]) cube([5,5,60]);
     translate([0, 0,  radiator_top]) E3Dv5_topFitting();
     translate([0, 0,             0]) E3Dv5_radiator();
     translate([0, 0,   -_tol-(5+5)]) E3Dv5_thermalBarrel();
    
     translate([0, 0,          -9.0]) E3Dv5_heater();
     translate([0, 0,         -25.0]) mk8M6Nozzle();
     //translate([0, 0,        -(1+2)]) E3Dv6_heater_v2();
     //translate([0, 0,-(11.5 + 8)]) E3Dv6_M6Nozzle();

}

module E3Dv5_topFitting(){
        translate([0,0,-1.0])
        difference(){
        union(){
          translate([0,0,0.5]) color("white")  
               cylinder(d=4,h=10); // PTFE tube
          translate([0,0,1]) color("Black")  translate([0,0,0]) 
               cylinder(d=7.0, h=2); // inlet
        }
         cylinder(d=2,h=11); // PTFE tube
        }
}

module mk8M6Nozzle(){
 difference(){
     color("Goldenrod") 
     union(){
     intersection(){
      union(){
          cylinder(d1=1.5,d2=7.5,h=4);
          translate([0,0,4])cylinder(d=7.5,h=4);
      }
      cylinder(d=7.5,h=8, $fn=6);
     }
      translate([0,0,8])cylinder(d=6,h=7);
     }
     cylinder(d=0.5,h=20);

 }
}

module E3Dv6_M6Nozzle(){
//
// h = 12.5
// dz to top of nut = 5
//
     difference(){
     color("DarkGoldenrod") union(){
          translate([ 0, 0, 0]) cylinder(d=1.00,d2=6.0,h=3);
          translate([ 0, 0, 2]) cylinder(d=8.00,h= 3.0,$fn=6);
          translate([ 0, 0, 5]) cylinder(d=5.00,h= 7.50);
          translate([ 0, 0, 7]) cylinder(d=6.00,h= 5.50);
     }
     translate([ 0, 0, -1]) cylinder(d=0.5,h=15);
     translate([ 0, 0, 6]) cylinder(d=2.0,h=10);
 }

}

module E3Dv5_thermalBarrel(){
// h = 22
    difference() {
        color("DarkGray")
        union() {
            cylinder(d=6,h= 5);
            cylinder(d=3,h=20);
            translate([ 0, 0, 7]) cylinder(d=6.9,h=9);
            translate([ 0, 0, 7]) cylinder(d=5.8,h=13);
        }
        translate([ 0, 0, -1])   cylinder(d=2.0,h=22);
        translate([ 0, 0, 14])   cylinder(d1=2.0,d2=4.1,h=2.1);
        translate([ 0, 0, 16])   cylinder(d=4.0,h=6);
    }
}

module E3Dv5_radiator(){
// h = 50.0
    radiator_fin_thick = 1.0;
    radiator_fin_dz    = (31/9);//2.5;
    radiator_shaft     = radiator_top - radiator_fittingHeight;

    difference(){

        color("lightgray") union(){

            cylinder(d1=12.5,d2=9,h=radiator_shaft );
            translate([ 0, 0, radiator_top-3.75 ]) cylinder(d=16,h= 3.75);
            translate([ 0, 0, radiator_shaft    ]) cylinder(d=12,h=radiator_fittingHeight); 
            translate([ 0, 0, radiator_shaft    ]) cylinder(d=16,h= 3.00); 
            translate([ 0, 0, radiator_shaft-radiator_fin_dz ]) cylinder(d=16,h= 1.00);

            for (i = [0:9]) {
                translate([0,0,radiator_fin_dz*i]) 
                    cylinder(d=radiator_diameter,h=radiator_fin_thick);
            }
        }

        translate([ 0, 0, -1 ])cylinder( d=6, h=radiator_top+2);
        translate([ 0, 0, 22 ])cylinder( d=4, h=radiator_top+2);
        translate([ 0, 0, 30 ])cylinder( d=8, h=radiator_top+2);

    }

    difference(){
        dh=3.75;
        color("Goldenrod") translate([ 0, 0, radiator_top-dh]) {
          intersection(){
              cylinder(d=8,h=dh); // top=50.0
              union(){
                 cylinder(d1=7,d2=7+dh,h=dh/2); // top=50.0
                 translate([ 0, 0, dh/2]) cylinder(d1=7+dh,d2=7,h=dh/2); // top=50.0
              }
          }
        }
        translate([ 0, 0, 22 ])cylinder( d=4, h=radiator_top+2);
    }

}


module E3Dv5_heater() {
    // h = 10
 {
blockX=15;
blockY=20;
blockZ=12;
 dely = 5;
rotate([0,0,180])
translate([ -blockX/2, -blockY/2-dely, -blockZ/2-2]) {
   
    color("Gray")
    difference(){
    chamferBox([blockX,blockY,blockZ],delta=0.4);
    translate([blockX/2,blockY/2+dely,-0.5]) 
            cylinder(d=6,h=blockZ+1);
    translate([blockX/2,blockY/2-7.5,-0.5]) 
           cylinder(d=2.1,h=blockZ+1);
    translate([-0.5,7.5,blockZ/2-0.5]) rotate([0,90,0]) 
           cylinder(d=6,h=blockX+1);
    translate([-0.5,-2,blockZ/2-0.5]) rotate([0,0,0]) 
           cube([blockX+1,8,0.75]);
    translate([blockX-blockX/3+0.5,blockY/2+4,5.5]) rotate([0,90,0]) 
           cylinder(d=2.5,h=blockY/3);
    }// diff

    if (_ShowFittings==1){
       color("DarkGray"){
        translate([blockX/2,blockY/2-7.5,-0.5]) 
            cylinder(d=2.1,h=blockZ+1);
       translate([-0.5,7.5,blockZ/2-0.5]) rotate([0,90,0]) 
             cylinder(d=6,h=blockX+1);
       translate([blockX-blockX/3+0.5,blockY/2+4,5.5]) rotate([0,90,0]) 
             cylinder(d=2.5,h=blockY/3);
       }
    }// fittings
    
    }// trans

}// gray

}


module E3Dv6_heater_v2(rotate=-90) {
// h = 11.5
    rotate([180,0,rotate]) difference() {
        color("Silver") union() {
            translate([-8,-8,0]) 
                chamferBox([16,23,11.5],delta=0.4);
        }
        // heated block
        translate([0,0,-1])     cylinder(d=5.50,h=14);
        translate([-9,6.5,7.5]) rotate([0,90,0]) cylinder(r=3.05, h=18);
        translate([-9,8,6.5])   cube([18,8.5,2]);
        translate([0,12.5,-1])  cylinder(r=1.4, h=13.5);
        translate([0,-6,5.75])  cylinder(r=1.2, h=6);
        translate([-9,-6,5.75]) rotate([0,90,0]) cylinder(r=1.55, h=18);
    }
}


module E3Dv5_fanMount(fan=0){

rotate([0,0,0])
translate([0,0,-20])  
    {

  union(){
   difference(){
    union(){
    hull(){
       translate([0, 3, 3]) roundedBox([40,40,7],2,true);
       translate([0, 3, 5]) rotate([90,0,0]) cube([36,6,38],center=true);
    }
    hull(){
       translate([0, 3, 5]) rotate([90,0,0]) cube([36,6,38],center=true);
       translate([0,1, 9]) rotate([90,0,0]) cube([36,6,36],center=true);
    }
    hull(){
       translate([0,1, 9]) rotate([90,0,0]) cube([36,6,36],center=true);
       translate([0,1,22]) rotate([90,0,0]) cube([30,6,32],center=true);
    }
    } // union
    
    d=40; // 40mm fan
    del=d/2.5; // for 30-50mm
    for (i=[-1,1]){
    for (j=[-1,1]){
        translate([i*del, j*del+3, -0.5]) cylinder(d=3.3,h=12,$fn=6);
    }
    }

    hull(){
    translate([0, 2, -1.0]) cylinder(d1=36,d2=radiator_diameter+0.25,h=20);
    translate([0, 7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=25);
    translate([0,-7, -0.5]) scale([1,0.7,1])cylinder(d1=radiator_diameter+0.25,d2=radiator_diameter+0.25-1,h=25);
    }
    translate([0, 25,20]) rotate([90,0,0]) cylinder(d=radiator_diameter+0.25,h=50);
  }// diff

     if (fan==1) {
       translate([0,3,-3]) 
       rotate([90,0,0]) 
           fan(40,6);
     }
    if (_ShowFittings==1){
        for (i=[-1,1]){
        for (j=[-1,1]){
            translate([i*16, j*16+3, -0.5-5]) socketBolt(size=3,length=12);
        }
        }
    }

    translate([-12.5, 0,25]) 
    rotate([0,40,0]) 
    rotate([90,0,0]) 
        roundedBox([3,5,2],1, true);
    translate([ 12.5, 0,25]) 
    rotate([0,40,0]) 
    rotate([90,0,0]) 
        roundedBox([5,3,2],1, true);
    
   } // union

  }// trans
}

