//======================================================================
//
// Aluminium 2020 Profile  Frame
// Cartesian Prusa i3 style
// Print volume, 250x250x140mm
// RSS
//
//======================================================================
//======================================================================
// Uses
//======================================================================

use <frame.scad>
use <xaxis.scad>
use <yaxis.scad>
use <zaxis.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <const.scad>

// print head location mockup

_xpos = 0;
_ypos = 0;
_zpos = 0;

_ShowHotend   = 1 ; // 1 or 0
_ShowCables   = 1 ; // 1 or 0
_ShowSwitches = 1 ; // 1 or 0

//======================================================================
// main program, comment components as needed
//======================================================================

translate([0,0,_prof]){

    translate([0,0,0]) liteFrame();
    translate([0,0,0]) feet();
    translate([0,0,0]) zDrive();
    translate([0,0,0]) yDrive();
    translate([0,0,0]) xDrive();
//    translate([0,0,0]) filamentExtruder();
}

//======================================================================
// modules
//======================================================================

module xDrive(){

    translate([0,_yoffxz-51.1+6,114+_zpos]) {
      xaxis(length=_xrods, endsep=_xendLength, barsep=_xrodZsep,
      px=(_platesize/2)-_xpos,
      depth=_rodoffdepth,
      car=_ShowHotend, dzr=0);
    }

    echo("length:",_xrods);
    echo("endsep:",_xendLength);
    echo("barsep:",_xrodZsep);
    echo("posx:",_xpos);
    echo("posz:",_zpos);
    echo("depth:",_rodoffdepth);

}

module yDrive(){
    yaxis(ypos=_ypos,cables=_ShowCables);
}

module zDrive(){
     zaxis(); // no zpos, offset applied to x axis
}

