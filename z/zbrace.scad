//
// MaxMicron frame brace
//
//======================================================================
// Uses
//======================================================================

use <std/fasteners.scad>

//======================================================================
// Parameters
// all dimensions in mm
//======================================================================

include <../const.scad>


width=_profile;
baseheight=_profile;
baselength=101.5;
armlength=baselength*1.4142135623731;

regd = (width == 20)?6.0:8.0;

_s = 8;

_ShowBolts = 1;

thick=3.0;

zbrace(dir=1);
//zbraceprint(dir=0);

module zbrace(dir=1){
    color([0.2,0.2,0.2,1.0])
    rotate([0,-135,0]) zbraceprint(dir=dir);
}

module zbraceprint(dir=0){

rotate([0,135,0])
rotate([0,0,-180*dir]){
mirror([dir,0,0]){
union(){
    
difference(){
union(){
    
basearm();
   translate([2.1,0,0]) sidearm();
}
   translate([80,width/2,-1])  boltHole(5, length= 30);
   translate([30,width/2,-1])  boltHole(5, length= 30);

}//d

if (_ShowBolts==1){
   translate([80,width/2,5])  rotate([0,180,0]) socketBolt(5, length= 12);
   translate([30,width/2,5])  rotate([0,180,0]) socketBolt(5, length= 12);
}

rotate([180,-90,0]) 
difference(){
union(){
translate([0,width,0])     vbasearm();
translate([2.1,width,0])   sidearm();
}
translate([80,width/2,0-1])    boltHole(5, length= 40);
translate([20,width/2,0-1])  boltHole(5, length= 140);
}//d

if (_ShowBolts==1){
    rotate([180,-90,0]) {
   translate([80,width/2,5])  rotate([0,180,0]) socketBolt(5, length= 12);
   translate([20,width/2,5])  rotate([0,180,0]) socketBolt(5, length= 12);
    }
}

translate([45,width/2,-1.95]) hull(){cylinder(d=regd,h=2); translate([-regd/2+20,0,0]) cylinder(d=regd,h=2);}
rotate([180,-90,0]){
translate([-5,width/2,-1.95]) hull(){cylinder(d=regd,h=2); translate([-regd/2+15,0,0]) cylinder(d=regd,h=2);}
translate([45,width/2,-1.95]) hull(){cylinder(d=regd,h=2); translate([-regd/2+15,0,0]) cylinder(d=regd,h=2);}
}
}//u

}
}
}


module slab4_col(p1, p2, p3, p4, r1) {
        r2=r1+r1;
    hull(){
        translate(p1) cube([r2,r2,r2],center=true);
        translate(p2) cube([r2,r2,r2],center=true);
    }
    hull(){
        translate(p1) cube([r2,r2,r2],center=true);
        translate(p3) cube([r2,r2,r2],center=true);
    }    
    hull(){
        translate(p1) cube([r2,r2,r2],center=true);
        translate(p4) cube([r2,r2,r2],center=true);
    }
    hull(){
        translate(p2) cube([r2,r2,r2],center=true);
        translate(p3) cube([r2,r2,r2],center=true);
    }
    hull(){
        translate(p2) cube([r2,r2,r2],center=true);
        translate(p4) cube([r2,r2,r2],center=true);
    }
   hull(){
        translate(p3) cube([r2,r2,r2],center=true);
        translate(p4) cube([r2,r2,r2],center=true);
    }
}


module slab4(p1, p2, p3, p4, r1) {
    hull(){
        r2=r1+r1;
        translate(p1) cube([r2,r2,r2],center=true);
        translate(p2) cube([r2,r2,r2],center=true);
        translate(p3) cube([r2,r2,r2],center=true);
        translate(p4) cube([r2,r2,r2],center=true);
    }
}

module basearm() {
union(){
slab4(
    [thick,thick-1.0*width,thick],
    [thick,width-thick,thick],
    [thick+baselength-2.0*thick,thick-1.0*width,thick],
    [thick+baselength-2.0*thick,width-thick,thick],
    thick
);
    
slab4(
    [thick, thick-1.0*width, thick],
    [thick,-thick-_tol,     -thick],
    [thick+baselength-2.0*thick,thick-1.0*width, thick],
    [thick+baselength-2.0*thick,-thick-_tol,    -thick],
    thick
);

} // union
} // basearm


module vbasearm() {
union(){
slab4(
    [-width+thick,thick-width-_tol,thick],
    [-width+thick,-thick+_tol,thick],
    [thick+baselength-2.0*thick,-thick+_tol,thick],
    [thick+baselength-2.0*thick,thick-width-_tol,thick],
    thick
);

} // union
} // basearm

module sidearm() {
_ra=thick*0.707;
union(){
translate([width,0.0,0.0])
rotate([0,-45,0]) {
 slab4_col(
    [                        _ra,      _ra-width, _ra],
    [                        _ra,           -_ra, _ra],
    [  _ra+0.4*armlength-2.0*_ra,      _ra-width, _ra],
    [  _ra+0.4*armlength-2.0*_ra,           -_ra, _ra],
    _ra
);
}// rotate

translate([2.5*width,0.0,0.0])
rotate([0,-45,0]) {
 slab4_col(
    [                        _ra,     _ra-width, _ra],
    [                        _ra,          -_ra, _ra],
    [ _ra+0.25*armlength-2.0*_ra,     _ra-width, _ra],
    [ _ra+0.25*armlength-2.0*_ra,          -_ra, _ra],
    _ra
);
} // rotate

translate([0.5*baselength-_ra,0.0,baselength*0.5])
rotate([0,45,0])  {
slab4_col(
    [                        _ra,     _ra-width, _ra],
    [                        _ra,          -_ra, _ra],
    [ _ra+0.5*armlength-2.0* _ra,     _ra-width, _ra],
    [ _ra+0.5*armlength-2.0* _ra,          -_ra, _ra],
     _ra
);
} // rotate

} //union

} // sidearm

