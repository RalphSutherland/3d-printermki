

_straightrod_diam    =   8.0;
_straightrod_offsetx = -22.0;
_straightrod_offsety =   -6.0; //mm 6.0 MaxMicron, HICtop

_leadscrew_diam = 8.0;
_leadscrew_x       = 0;//(0.5*_xsize) - 20.25;
_leadscrew_y       = 0;//53.25-(0.5*_ysize);

module r_end(){
color("red") translate([_leadscrew_x,_leadscrew_y,0])  cylinder(d=_leadscrew_diam,h=305);

color("green") translate([_straightrod_offsetx,_straightrod_offsety,0])   cylinder(d=_straightrod_diam,h=305);

}

 r_end();