use <std/boxes.scad>
use <std/fasteners.scad>
use <std/motors.scad>
use <zrods.scad>

$fn  = 80;
_tol = 0.2;

_moc=21.15; // 42.3 total motor center
_thk=6.0;

_profile = 20.0; // 20-20 al profile mount

_straightrod_diam    =    8.0;
_straightrod_offsetx =   22.0;
_straightrod_offsety =    6.0; //mm 6.0 MaxMicron, HICtop

_hole    = 5; //M5 mounting bolt holes

_topxsize    = 50.0 ; // mm
_topysize    = 50.0+_profile ; // mm
_topzsize    =  5.0 ; // mm
_topcorner   = 12.0 ; // mm radius of rounded corners

_bearing_diameter  = 22.0; // mm
_bearing_depth     = 7.0; // mm

_leadscrew_diam    = 8.0;
_leadscrew_x       = (0.5*_topxsize) - _moc ; // 20.25
_leadscrew_y       = 54.0 -_moc+_profile+_tol -(0.5*_topysize); 
// calc from 54mm lower offset = 53.1 -(0.5*_ysize)// measured 53.25-(0.5*_ysize);

_minx     = 2*(_moc) + _profile+1;
_maxx     = _moc + _profile + (_straightrod_offsetx)+_straightrod_diam+1;
_xsize    = (_maxx > _minx)? _maxx: _minx;// mm
_ysize    = 2*(_moc) ; // mm
_ysize2   = 2*(_moc)+12 ; // mm
_xsize2   = 2*_profile ; // mm
_zsize    = 7.0 ; // mm
_corner =   3.0   ; // mm radius of rounded corners

_showChainAnchor = 1;
//import("ZMotor-R2.stl",convexity=6);

//zplate(p=_profile);
//mirror([1,0,0]) zplate();
//translate([_straightrod_offsetx,_straightrod_offsety,-1]) r_end();
//translate([0,0,300]) rotate([0,0,180]) ztopplate(1);
ztopplate(1);

module zplateleft(p=_profile){
    zplate(p);
}
module zplateright(p=_profile){
    mirror([1,0,0]) zplate(p);
}

module zplate(p=_profile){
rotate([0,0,180])translate(-[p + _moc + 1,_moc,0])
color("Red") 
difference(){
    
         union(){
              translate([_xsize/2,_ysize/2,_zsize/2]) roundedBox([_xsize,_ysize,_zsize],_corner,true);
              translate([_xsize2/2,_ysize2/2,_zsize/2]) roundedBox([_xsize2,_ysize2,_zsize],_corner,true);
              translate([p/2+_xsize2/2,_ysize2-2,20]) roundedBox([p,4,40],2,true);
         }
         
translate([p+_moc+1,_moc,-1]) stepper_motor_mount(17,slide_distance=0, mochup=false, tolerance=_tol);
translate([p+_moc+_straightrod_offsetx+1,_moc+_straightrod_offsety,-1]) cylinder(d=_straightrod_diam+_tol, h=_zsize+2);
translate([p+_moc+1,_moc,-1]) cylinder(d=24,h=_zsize+2);

      // M5 bolts
      translate([p/2,(_ysize-24)/2,-1])  boltHole(_hole,length=_zsize+3,tolerance=_tol);
      translate([p/2,(_ysize-24)/2, 6]) cylinder(d=9.0+_tol,h=5);
      translate([p/2,_ysize-(_ysize-24)/2,-1])  boltHole(_hole,length=_zsize+3,tolerance=_tol);
      translate([p/2,_ysize-(_ysize-24)/2, 6])  cylinder(d=9.0+_tol,h=5);
    

}

}

//======================================================================
// modules
//======================================================================
module ztopplate( right=0 ){
    
color("Red") 
    translate([-_leadscrew_x,-_leadscrew_y,(0.5*_topzsize)+2]) 
    difference(){
        
     union(){
        hull(){
          roundedBox([_topxsize,_topysize,_topzsize ],_corner,true);
          translate([0,-_topysize*0.25,0])
              roundedBox([_topxsize,_topysize*0.5,_topzsize ],_topcorner*0.25,true);
         // translate([0,0,1]) roundedBox([_topxsize-5,_topysize-5,_topzsize ],_topcorner*0.5,true);
       }
       translate([0,0.5*(_profile+_tol),-2]) 
           roundedBox([_topxsize,_topysize-_profile-_tol,_topzsize ],_corner,true);
       translate([0,-0.5*(_topysize-_profile)+_profile+_tol,-2]) 
           cube([_topxsize,_profile-_tol,_topzsize ],true);
    }
    
     dx = ( right == 1)?1.0:-1.0;
    
      union(){
          translate([dx*_leadscrew_x,_leadscrew_y,-0.5*_topzsize-5]) 
              cylinder(d=_leadscrew_diam+1+_tol*2,h=_topzsize+10.0);
          translate([dx*_leadscrew_x,_leadscrew_y,(0.5*_topzsize)+1-_bearing_depth]) 
              cylinder(d=_bearing_diameter+_tol*2,h=_topzsize+10.0);
      }
      translate([dx*(_leadscrew_x-_straightrod_offsetx),_leadscrew_y-_straightrod_offsety,-0.5*_topzsize-5]) 
         cylinder(d=_straightrod_diam+_tol*2, h=_topzsize+10.0); // straight rod hole

      if (_showChainAnchor > 0){
//chain guide attachment point if needed
      translate([dx*10,-3,-0.5*_topzsize-5]) 
          boltHole(_hole,length=20);
      }
 
      // M5 bolts
      translate([-0.25*_topxsize-2, -0.5*(_topysize-_profile), -0.5*_zsize-1])  boltHole(_hole,length=_topzsize+3);
      translate([-0.25*_topxsize-2, -0.5*(_topysize-_profile), 1]) cylinder(d=9.0+_tol,h=5);
      translate([ 0.25*_topxsize+2, -0.5*(_topysize-_profile), -0.5*_zsize-1])  boltHole(_hole,length=_topzsize+3);
      translate([ 0.25*_topxsize+2, -0.5*(_topysize-_profile), 1]) cylinder(d=9.0+_tol,h=5);

      }
}
