// 20-20 profile HICTop/MaxMicron Z-axis top plate
// RSS 2016
//
use <std/boxes.scad>
use <std/fasteners.scad>
//use <chain_port.scad>

//======================================================================
// Parameters
//======================================================================


_TOL   = 0.2;

_profile = 20.0; // 20-20 al profile mount
_hole    = 5; //M5

_moc    = 21.1 ;// motor center, 42.2*0.5

_xsize    = 50.0 ; // mm
_ysize    = 50.0+_profile ; // mm
_zsize    =  5.0 ; // mm
_corner   = 12.0 ; // mm radius of rounded corners

_bearing_diameter = 22.0; // mm
_bearing_depth     = 7.0; // mm

_leadscrew_diam = 8.0;
_leadscrew_x       = (0.5*_xsize) - _moc ; // 20.25
_leadscrew_y       = 54.0 -_moc+_profile+_TOL -(0.5*_ysize); // calc from 54mm lower offset = 53.1 -(0.5*_ysize)// measured 53.25-(0.5*_ysize);

_straightrod_diam    =   8.0;
_straightrod_offsetx = -22.0;
_straightrod_offsety =  -6.0; //mm 6.0 MaxMicron, HICtop

//======================================================================
// main program, comment components as needed
//======================================================================

ztopplate(0);
//translate([0.25*_xsize-3,-6,1+_zsize*0.5]) chain_link_notop(0,1);

//======================================================================
// modules
//======================================================================
module ztopplate( right ){
    translate([-_leadscrew_x,-_leadscrew_y,(0.5*_zsize)+2]) 
    difference(){
        
     union(){
        hull(){
          roundedBox([_xsize,_ysize,_zsize ],_corner,true);
          translate([0,-_ysize*0.25,0])roundedBox([_xsize,_ysize*0.5,_zsize ],_corner*0.25,true);
         // translate([0,0,1]) roundedBox([_xsize-5,_ysize-5,_zsize ],_corner*0.5,true);
       }
        translate([0,0.5*(_profile+_TOL),-2]) roundedBox([_xsize,_ysize-_profile-_TOL,_zsize ],_corner,true);
        translate([0,-0.5*(_ysize-_profile)+_profile+_TOL,-2]) cube([_xsize,_profile-_TOL,_zsize ],true);
    }
    
     dx = ( right == 1)?1.0:-1.0;
    
      union(){
          translate([dx*_leadscrew_x,_leadscrew_y,-0.5*_zsize-5]) cylinder(d=_leadscrew_diam+1+_TOL*2,h=_zsize+10.0);
           translate([dx*_leadscrew_x,_leadscrew_y,(0.5*_zsize)+1-_bearing_depth]) cylinder(d=_bearing_diameter+_TOL*2,h=_zsize+10.0);
      }
      translate([dx*(_leadscrew_x+_straightrod_offsetx),_leadscrew_y+_straightrod_offsety,-0.5*_zsize-5]) cylinder(d=_straightrod_diam+_TOL*2, h=_zsize+10.0); // straight rod hole
 
      // M5 bolts
      translate([-0.25*_xsize-2, -0.5*(_ysize-_profile), -0.5*_zsize-1])  boltHole(_hole,length=_zsize+3);
      translate([-0.25*_xsize-2, -0.5*(_ysize-_profile), 1]) cylinder(d=9.0+_TOL,h=5);
      translate([ 0.25*_xsize+2, -0.5*(_ysize-_profile), -0.5*_zsize-1])  boltHole(_hole,length=_zsize+3);
      translate([ 0.25*_xsize+2, -0.5*(_ysize-_profile), 1]) cylinder(d=9.0+_TOL,h=5);

      }
}
